import axios from 'axios';
import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function EditGuru() {

    const [show, setShow] = useState(false);
    const history = useHistory();
    const param = useParams();
    const [nama, setNama] = useState("");
    const [jekel, setJekel] = useState("");
    const [noHp, setnoHp] = useState("");
    const [alamat, setAlamat] = useState("");
    const [nisn, setNisn] = useState ("");

    const put = async (e) => {
        e.preventDefault();
        e.persist();
    
        try {
          await axios.put(
            "http://localhost:2026/guru/" + param.id, {
            nama:nama,
            jekel:jekel,
            noHp:noHp,
            alamat:alamat,
            nisn:nisn,
        });
        setShow(false);
          Swal.fire({
            icon: "success",
            title: "berhasil mengedit",
            showConfirmButton: false,
            timer: 1500,
          });
        setTimeout(() => {
          history.push("/guru");
          window.location.reload();
        }, 1500);
        } catch (err) {
          console.log(err);
        }
      };
    
      useEffect(() => {
        axios
          .get("http://localhost:2026/guru/" + param.id)
          .then((response) => {
            const akademik = response.data.data;
            setNama(akademik.nama);
            setJekel(akademik.jekel);
            setnoHp(akademik.noHp);
            setAlamat(akademik.alamat);
            setNisn(akademik.nisn);
          })
          .catch((error) => {
            alert("Terjadi Kesalahan " + error);
          });
      }, [param.id]);
  return (
    <div>
    <div className="shadow-2xl mt-5 md:w-[35%] md:ml-[30%] py-6 lg:px-8">
    <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-black">
      Edit
    </h3>
    <form onSubmit={put} className="space-y-3">
      <div>
        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
         NISN
        </label>
        <input
          placeholder="NISN"
          onChange={(e) => setNisn(e.target.value)}
          value={nisn}
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
          required
        />
      </div>
      <div>
        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
          nama
        </label>
        <input
          placeholder="Nama"
          onChange={(e) => setNama(e.target.value)}
          value={nama}
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
          required
        />
      </div>
      <div>
        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
          jekel
        </label>
        <input
          placeholder="jekel"
          onChange={(e) => setJekel(e.target.value)}
          value={jekel}
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
          required
        />
      </div>
      <div>
        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
          no handphone
        </label>
        <input
          placeholder="no handphone"
          onChange={(e) => setnoHp(e.target.value)}
          value={noHp}
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
          required
        />
      </div>
      <div>
        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
          Alamat
        </label>
        <input
          placeholder="Alamat"
          onChange={(e) => setAlamat(e.target.value)}
          value={alamat}
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
          required
        />
      </div>
      <button
        type="submit"
        className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
      >
        Simpan
      </button>
    </form>
  </div>
  </div>
  )
}
