import axios from "axios";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function ProfileEdit() {
  const param = useParams();
  const [email, setEmail] = useState("");
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [password, setPassword] = useState("");
  const [noTelepon, setNoHp] = useState("");
  const [alamatSekolah, setAlamatSekolah] = useState("");
  const [namaSekolah, setNamaSekolah] = useState("");
  const [role, setRole] = useState("");
  const [username, setUsername] = useState("");
  const [photoProfile, setPhotoProfile] = useState("");
  const [profile, setProfile] = useState({
    nama: "",
    email: "",
    noTelepon: "",
    password: "",
    alamatSekolah: "",
    namaSekolah: "",
    // photoProfile: null,
  });

  useEffect(() => {
    axios
      .get("http://localhost:2026/sekolah/" + localStorage.getItem("userId"))
      .then((response) => {
        const profil = response.data.data;
        // setPhotoProfile(profile.photoProfile);
        setNamaSekolah(profil.namaSekolah);
        setAlamatSekolah(profil.alamatSekolah);
        setEmail(profil.email);
        setNoHp(profil.noTelepon);
        setPassword(profil.password);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);

  console.log(noTelepon);

  const putProfile = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("alamatSekolah", alamatSekolah);
    formData.append("namaSekolah", namaSekolah);
    formData.append("email", email);
    formData.append("noTelepon", noTelepon);
    console.log(formData);

    try {
      await axios.put(
        `http://localhost:2026/sekolah/` + localStorage.getItem("userId"), formData
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "berhasil mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <div className="shadow-2xl mt-5 md:w-[35%] md:ml-[30%] py-6 lg:px-8">
        <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-black">
          Edit
        </h3>
        <form onSubmit={putProfile} className="space-y-3">
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
              Alamat Sekolah
            </label>
            <input
              placeholder="Alamat Sekolah"
              type="text"
              value={alamatSekolah}
              onChange={(e) => setAlamatSekolah(e.target.value)}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          {/* <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
              UserName
            </label>
            <input
              placeholder="UserName"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div> */}
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
              Nama Sekolah
            </label>
            <input
              placeholder="Nama Sekolah"
              value={namaSekolah}
              onChange={(e) => setNamaSekolah(e.target.value)}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
              Email
            </label>
            <input
              placeholder=" Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
              No Handphone
            </label>
            <input
              placeholder=" No HandPhone"
              value={noTelepon}
              onChange={(e) => setNoHp(e.target.value)}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          <button
            type="submit"
            className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          >
            Simpan
          </button>
        </form>
      </div>
    </div>
  );
}
