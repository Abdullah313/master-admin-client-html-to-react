import axios from 'axios';
import React, { useState } from 'react'
import { useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function EditPendaftaranSiswa() {
    const [show, setShow] = useState(false);
    const param = useParams();
    const history = useHistory();

    const [nama, setNama] = useState("");
    const [jenisKelamin, setJenisKelamin] = useState("");
    const [tempatLahir, setTempatLahir] = useState("");
    const [tglLahir, setTglLahir] = useState("");
    const [tahunAjaran, setTahunAjaran] = useState("");
    const [jenjang, setJenjang] = useState("");
    const [alamat, setAlamat] = useState("");
    const [ no_reg, setNoReg] = useState("");

    const Put = async (e, idx) => {
        e.preventDefault();

        const data = new FormData()
        data.append("nama", nama);
        data.append("jenjang", jenjang);
        data.append("jenisKelamin", jenisKelamin);
        data.append("tglLahir", tglLahir);
        data.append("tahunAjaran", tahunAjaran);
        data.append("alamat", alamat);
    
        try {
          await axios.put(`http://localhost:2026/pendaftaran_siswa/` + param.id, data);
          setShow(false);
          Swal.fire({
            icon: "success",
            title: "berhasil mengedit",
            showConfirmButton: false,
            timer: 1500,
          });
          console.log(nama);
          console.log(jenjang);
          console.log(jenisKelamin);
          console.log(tglLahir);
          console.log(tahunAjaran);
          console.log(alamat);
          // setTimeout(() => {
          //   history.push("/pendaftaransiswa");
          //   window.location.reload();
          // }, 1500);
        } catch (error) {
          console.log(error);
        }
      };

      useEffect(() => {
        axios
          //get untuk menampilkan data
          .get("http://localhost:2026/pendaftaran_siswa/" + param.id)
          .then((response) => {
            const akademik = response.data.data;
            setNama(akademik.nama);
            setJenisKelamin(akademik.jenisKelamin);
            setTglLahir(akademik.tglLahir);
            setTempatLahir(akademik.tempatLahir);
            setTahunAjaran(akademik.tahunAjaran);
            setJenjang(akademik.jenjang);
            setAlamat(akademik.alamat);
          })
          .catch((error) => {
            alert("Terjadi Kesalahan Sir!" + error);
          });
      }, [param.id]);
  return (
    <div>
        <div className="shadow-2xl mt-5 md:w-[35%] md:ml-[30%] py-6 lg:px-8">
        <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-black">
          Edit
        </h3>
        <form onSubmit={Put} className="space-y-3">
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
             Nama
            </label>
            <input
              placeholder="Nama"
              onChange={(e) => setNama(e.target.value)}
              value={nama}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
              Jekel
            </label>
            <input
              placeholder="Jekel"
              onChange={(e) => setJenisKelamin(e.target.value)}
              value={jenisKelamin}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
              Alamat
            </label>
            <input
              placeholder="Alamat"
              onChange={(e) => setAlamat(e.target.value)}
              value={alamat}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
              Tempat Lahir
            </label>
            <input
              placeholder="Tempat Lahir"
              onChange={(e) => setTempatLahir(e.target.value)}
              value={tempatLahir}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
              Jenjang
            </label>
            <input
              placeholder="Jenjang"
              onChange={(e) => setJenjang(e.target.value)}
              value={jenjang}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
              Tanggal Lahir
            </label>
            <input
              placeholder="Tanggal Lahir"
              onChange={(e) => setTglLahir(e.target.value)}
              value={tglLahir}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
              Tahun Ajaran
            </label>
            <input
              placeholder="Tahun Ajaran"
              onChange={(e) => setTahunAjaran(e.target.value)}
              value={tahunAjaran}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          <button
            type="submit"
            className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          >
            Simpan
          </button>
        </form>
      </div>
    </div>
  )
}
