import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordType, setPasswordType] = useState("password");

  const togglePassword = () => {
    if (passwordType === "password") {
      setPasswordType("text");
      return;
    }
    setPasswordType("password");
  };

  const history = useHistory();

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:2026/sekolah/login",
        {
          email: email,
          password: password,
        }
      );
      // Jika respon 200/ ok
      if (status === 200) {
        Swal.fire({
          title: "Login Berhasil!",
          icon: "success",
          showConfirmButton: false,
          timer: 1500,
        }).then(function () {
          localStorage.setItem("userId", data.data.user.id);
          localStorage.setItem("role", data.data.user.role);
          console.log("/home");
          history.push("/home");
        });
      }

      // const { data, status } = await axios.post(
      //   "http://localhost:2026/sekolah/login",
      //   {
      //     email: email,
      //     username: username,
      //     password: password,
      //   }
      // );
      // Jika respon 200/ ok
      // if (status === 200) {
      //   Swal.fire({
      //     icon: "success",
      //     title: "Login Berhasil!!!",
      //     showConfirmButton: false,
      //     timer: 1500,
      //   });
      //   localStorage.setItem("userId", data.data.user.id);
      //   localStorage.setItem("token", data.data.token);
      //   localStorage.setItem("role", data.data.user.role);
      //   history.push("/dashboard");
      //   setTimeout(() => {
      //     window.location.reload();
      //   }, 1500);
      // }
    } catch (error) {
      Swal.fire({
        title: "Login Gagal!",
        text: "Email atau password salah",
        icon: "error",
        confirmButtonText: "OK",
      }).then(function () {
        window.location.reload();
      });
      console.log(error);
    }
  };
  return (
    <div>
      <section className="vh-100" style={{ backgroundColor: "#4169e1" }}>
        <div className="container  h-100">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col col-xl-10">
              <div className="card" style={{ borderRadius: "1rem" }}>
                <div className="row g-0">
                  <div className="col-md-6 col-lg-5 d-none d-md-block">
                    <img
                      src="https://images.unsplash.com/photo-1599634875158-597d3f647df6?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=375&q=80"
                      alt="login form"
                      className="img-fluid"
                      style={{ borderRadius: "1rem 0 0 1rem" }}
                    />
                  </div>
                  <div className="col-md-6 col-lg-7 d-flex align-items-center">
                    <div className="card-body p-4 p-lg-5 text-black">
                      <form onSubmit={login} action="">
                        <div className="d-flex align-items-center mb-3 pb-1">
                          <img
                            src="https://banner2.cleanpng.com/20180325/rgq/kisspng-education-school-computer-icons-clip-art-coin-5ab83c5be030d4.7184362115220235159183.jpg"
                            width={50}
                            height={50}
                            className="d-inline-block align-text-top me-3"
                          />
                          <span className="h2 fw-bold mb-0 mx-2">
                            Sistem Informasi Sekolah
                          </span>
                        </div>
                        <h5
                          className="fw-normal mb-3 pb-3"
                          style={{ letterSpacing: 1 }}
                        >
                          Silahkan login ke akun anda
                        </h5>
                        {/* <div className="form-outline mb-4">
                          <label
                            className="form-label"
                            htmlFor="form2Example17"
                          >
                            Username
                          </label>
                          <input
                            id="form2Example17"
                            type="full name"
                            name="username"
                            className="form-control form-control-lg"
                            placeholder="Username"
                            required
                            oninvalid="this.setCustomValidity('Tidak boleh kosong')"
                            oninput="setCustomValidity('')"
                          />
                        </div> */}
                        <div className="form-outline mb-4">
                          <label
                            className="form-label"
                            htmlFor="form2Example17"
                          >
                            USERNAME or Email
                          </label>
                          <input
                            id="form2Example17"
                            type="text"
                            name="email"
                            className="form-control form-control-lg"
                            placeholder="Enter USERNAME or Email"
                            onChange={(e) => setEmail(e.target.value)}
                            required
                            oninvalid="this.setCustomValidity('Tidak boleh kosong')"
                            // oninput="setCustomValidity('')"
                            autocomplete="off"
                          />
                        </div>
                        <div className="form-outline">
                          <label
                            className="form-label"
                            htmlFor="form2Example27"
                          >
                            Password
                          </label>
                          <div className="d-flex align-items-center">
                            <input
                              id="form2Example27"
                              value={password}
                              type={passwordType}
                              name="password"
                              className="form-control "
                              placeholder="Enter Password"
                              required
                              oninvalid="this.setCustomValidity('Tidak boleh kosong')"
                              oninput="setCustomValidity('')"
                              onChange={(e) => setPassword(e.target.value)}
                            />
                            <span
                              onClick={togglePassword}
                              class="order-3 p-2 bg-secondary "
                            >
                              <i class="far fa-eye-slash"></i>
                            </span>
                          </div>
                        </div>
                        <div className="pt-1 mb-4">
                          <button
                            className="btn btn-dark btn-lg btn-block"
                            type="submit"
                          >
                            Login
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* /.login-box */}
    </div>
  );
}

export default Login;
