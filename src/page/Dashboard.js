import React from "react";
import "../App.css";

function Dashboard() {
  return (
    <div className="content-wrapper">
      <div className="container-fluid ">
        <div className="circle"></div>
        <div className="box">
          <section className="text">
            <h1>SELAMAT DATANG, MIN</h1>
            <p>
              Yuk, semangat mengelola sekolah bareng <span>SIS</span>{" "}
            </p>
          </section>
          <section className="img-box">
            <img className="school" src="dist/img/school.png" />
          </section>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
