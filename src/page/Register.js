import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "../App.css";
import Swal from "sweetalert2";

function Register() {
  const [namaSekolah, setNamaSekolah] = useState("");
  const [alamatSekolah, setAlamatSekolah] = useState("");
  const [noTelepon, setNoTelepon] = useState("");
  const [email, setEmail] = useState("");
  // const [role, setRole] = useState("ADMIN"); //atau bisa gk usah pake setter getter kalau role otomatis ADMIN
  const history = useHistory();
  // CSS Tambahan
  const myStyle = {
    fontSize: "14px",
  };
  const [password, setPassword] = useState("");
  const [passwordType, setPasswordType] = useState("password");

  const togglePassword = () => {
    if (passwordType === "password") {
      setPasswordType("text");
      return;
    }
    setPasswordType("password");
  };
  const register = async (e) => {
    e.preventDefault();
    try {
      await axios
        .post("http://localhost:2026/sekolah/register", {
          namaSekolah: namaSekolah,
          alamatSekolah: alamatSekolah,
          noTelepon: noTelepon,
          email: email,
          password: password,
          role: "ADMIN", // => tadi sempat error 400 karena error client jadi saat post ada yg salah inputnya, bila role bukan suatu pilihan makan harus tetep diisi manual
          // role: role, // => ADMIN ditulis di bagian atas di setter getter dan useState di beri ADMIN
        })
        .then(() => {
          Swal.fire({
            icon: "success",
            title: "Berhasil Registrasi",
            showConfirmButton: false,
            timer: 1500,
          });
          setTimeout(() => {
            history.push("/");
            window.location.reload();
          }, 1500);
        });
    } catch (error) {
      alert("Terjadi Kesalahan " + error);
    }
  };
  return (
    <div>
      <section className="vh-100" style={{ backgroundColor: "#4169e1" }}>
        <div className="container  h-100">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col col-xl-10">
              <div className="card" style={{ borderRadius: "1rem" }}>
                <div className="row g-0">
                  <div className="col-md-6 col-lg-5 d-none d-md-block ">
                    <img
                      src="https://images.unsplash.com/photo-1599634875158-597d3f647df6?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=375&q=80"
                      alt="login form"
                      className="img-fluid h-100"
                      style={{ borderRadius: "1rem 0 0 1rem" }}
                    />
                  </div>
                  <div className="col-md-6 col-lg-7 ">
                    <div className="card-body  text-black ">
                      <form onSubmit={register} action="">
                        <div className="d-flex align-items-center mb-3 pb-1">
                          <img
                            src="https://banner2.cleanpng.com/20180325/rgq/kisspng-education-school-computer-icons-clip-art-coin-5ab83c5be030d4.7184362115220235159183.jpg"
                            width={50}
                            height={50}
                            className="d-inline-block align-text-top me-3"
                          />
                          <span className="h2 fw-bold mb-0 mx-2">
                            Sistem Informasi Sekolah
                          </span>
                        </div>
                        <h5
                          className="fw-normal mb-2 pb-2"
                          style={{ letterSpacing: 1 }}
                        >
                          Silahkan Registrasi Terlebih Dahulu
                        </h5>
                        <div className="form-outline">
                          <label
                            className="form-label"
                            htmlFor="form2Example17"
                          >
                            Nama Sekolah
                          </label>
                          <input
                            style={myStyle}
                            id="form2Example17"
                            type="text"
                            name="username"
                            className="form-control "
                            placeholder="Username"
                            required
                            oninvalid="this.setCustomValidity('Tidak boleh kosong')"
                            oninput="setCustomValidity('')"
                            value={namaSekolah}
                            onChange={(e) => setNamaSekolah(e.target.value)}
                          />
                        </div>

                        <div className="form-outline ">
                          <label
                            className="form-label"
                            htmlFor="form2Example27"
                          >
                            Alamat Sekolah
                          </label>
                          <input
                            style={myStyle}
                            id="form2Example27"
                            type="text"
                            name="alamat sekolah"
                            className="form-control "
                            placeholder="Alamat Sekolah"
                            required
                            oninvalid="this.setCustomValidity('Tidak boleh kosong')"
                            oninput="setCustomValidity('')"
                            value={alamatSekolah}
                            onChange={(e) => setAlamatSekolah(e.target.value)}
                          />
                        </div>
                        <div className="form-outline">
                          <label
                            className="form-label"
                            htmlFor="form2Example27"
                          >
                            Nomor Telepon
                          </label>
                          <input
                            style={myStyle}
                            id="form2Example27"
                            type="text"
                            name="nomor telepon"
                            className="form-control "
                            placeholder="Nomor Telepon"
                            required
                            oninvalid="this.setCustomValidity('Tidak boleh kosong')"
                            oninput="setCustomValidity('')"
                            value={noTelepon}
                            onChange={(e) => setNoTelepon(e.target.value)}
                          />
                        </div>
                        <div className="form-outline">
                          <label
                            className="form-label"
                            htmlFor="form2Example27"
                          >
                            Email
                          </label>
                          <input
                            style={myStyle}
                            id="form2Example27"
                            type="email"
                            name="email"
                            className="form-control "
                            placeholder="Enter Email"
                            required
                            oninvalid="this.setCustomValidity('Tidak boleh kosong')"
                            oninput="setCustomValidity('')"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                          />
                        </div>

                        <div className="form-outline">
                          <label
                            className="form-label"
                            htmlFor="form2Example27"
                          >
                            Password
                          </label>
                          <div className="d-flex align-items-center">
                            <input
                              style={myStyle}
                              id="form2Example27"
                              value={password}
                              type={passwordType}
                              name="password"
                              className="form-control "
                              placeholder="Enter Password"
                              required
                              oninvalid="this.setCustomValidity('Tidak boleh kosong')"
                              oninput="setCustomValidity('')"
                              onChange={(e) => setPassword(e.target.value)}
                            />
                            <span
                              onClick={togglePassword}
                              class="order-3 p-2 bg-secondary "
                            >
                              <i class="far fa-eye-slash"></i>
                            </span>
                          </div>
                        </div>

                        {/* <div>
                          <label for="password" class="sr-only">
                            Password
                          </label>

                          <div class="relative">
                            <input
                              value={password}
                              type={passwordType}
                              class="md:h-5 w-full rounded-lg border p-4 pr-12 text-sm shadow-sm"
                              placeholder="Enter password"
                              onChange={(e) => setPassword(e.target.value)}
                            />

                            <span
                              onClick={togglePassword}
                              class="absolute inset-y-0 right-0 grid place-content-center px-4"
                            >
                              <i class="far fa-eye-slash"></i>
                            </span>
                          </div>
                        </div> */}
                        <div className="pt-1">
                          <button
                            className="btn btn-dark btn-sm btn-block"
                            type="submit"
                          >
                            Registrasi
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* /.login-box */}
    </div>
  );
}

export default Register;
