import axios from "axios";
import React, { useState } from "react";
import { useEffect } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import Navbar from "../component/Navbar";
import Sidebar from "../component/Sidebar";
// import "../Akun/Profile.css";

export default function Profile() {
  const param = useParams();
  const [email, setEmail] = useState("");
  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);
  const handleClose = () => setShow(false);
  const handleClose1 = () => setShow1(false);
  const handleShow = () => setShow(true);
  const handleShow1 = () => setShow1(true);
  const [password, setPassword] = useState("");
  const [username, setNama] = useState("");
  const [noTelepon, setNoHp] = useState("");
  const [alamatSekolah, setAlamatSekolah] = useState("");
  const [namaSekolah, setNamaSekolah] = useState("");
  const [foto, setPhotoProfile] = useState(null);
  const [profile, setProfile] = useState({
    username: "",
    email: "",
    noTelepon: "",
    password: "",
    alamatSekolah: "",
    namaSekolah: "",
    foto:null,
  });

  const getAll = async () => {
    await axios
      .get("http://localhost:2026/sekolah/" + localStorage.getItem("userId"))
      .then((res) => {
        setProfile(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  useEffect(() => {
    axios
      .get("http://localhost:2026/sekolah/" + localStorage.getItem("userId"))
      .then((response) => {
        const profil = response.data.data;
        setPhotoProfile(profile.photoProfile);
        setNamaSekolah(profil.namaSekolah);
        setAlamatSekolah(profil.alamatSekolah);
        setEmail(profil.email);
        setNoHp(profil.noHp);
        setPassword(profil.password);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);

  const putProfile = async (e) => {
    e.preventDefault();
    e.persist();

    // const formData = new FormData();
    // formData.append("alamatSekolah", alamatSekolah);
    // formData.append("namaSekolah", namaSekolah);
    // formData.append("email", email);
    // formData.append("noTelepon", noTelepon);
    // console.log(formData);

    const data = {
      alamatSekolah:alamatSekolah,
      namaSekolah:namaSekolah,
      email:email,
      noTelepon:noTelepon
    }

  try {
    await axios.put(
      `http://localhost:2026/sekolah/${localStorage.getItem("userId")}`, data
    );
    setShow(false);
    Swal.fire({
      icon: "success",
      title: "berhasil mengedit",
      showConfirmButton: false,
      timer: 1500,
    });
    setTimeout(() => {
      window.location.reload();
    }, 1500);
  } catch (error) {
    console.log(error);
  }
};

const put = async (e) => {
  e.preventDefault();
  e.persist();

  const data = new FormData();
  data.append("file", foto);
  
  console.log(foto);
  try {
    await axios.put(
      `http://localhost:2026/sekolah/foto/${localStorage.getItem("userId")}`, data
    );
    setShow(false);
    Swal.fire({
      icon: "success",
      title: "berhasil mengedit",
      showConfirmButton: false,
      timer: 1500,
    });
    setTimeout(() => {
      window.location.reload();
    }, 1500);
  } catch (err) {
    console.log(err);
  }
};

const put1 = async (e) => {
  e.preventDefault();
  e.persist();

  const formData = new FormData();
  formData.append("password", password);
  try {
    await axios.put(
      `http://localhost:2026/sekolah/password/${localStorage.getItem("userId")}?password=${password}`,
      formData
    );
    setShow(false);
    Swal.fire({
      icon: "success",
      title: "berhasil mengedit",
      showConfirmButton: false,
      timer: 1500,
    });
    setTimeout(() => {
      window.location.reload();
    }, 1500);
  } catch (err) {
    console.log(err);
  }
};

  return (
    <div>
        {localStorage.getItem("userId") !== null ? (
       <>
        <Navbar/>
        <div className="flex md:gap-x-[6%]">
            <Sidebar/>
      <div className=" p-3 pt-2" onSubmit={putProfile}>
        <div className="md:flex bg-gray-100 p-[4rem] gap-x-[3rem] md:min-w-[50rem] min-w-[28rem] md:max-w-[50rem]  ml-12 border-blue-200 border-2 pb-0 shadow-2xl rounded-tl-3xl rounded-br-3xl back mt-20">
          <div>
            <button className="bg-blue-400 text-white font-bold py-2 px-4 md:-ml-[4rem] md:mt-0 md:inline hidden rounded-r-full" onClick={handleShow1}>
              Password
            </button>

            <img
              className="md:min-w-[15rem] max-w-[12rem] md:max-w-[17rem] md:ml-9 ml-9 md:min-h-[17rem] min-h-[14rem]  md:rounded-b-none rounded-b-full md:mt-0 -mt-16 border-3 border-blue-800 border md:rounded-t-full"
              src={profile.foto}
              alt=""
            />
          </div>
          <div className="md:-mt-16 mt-10 h-[20rem] md:rounded-b-full md:bg-[#C0EEF2] w-[20rem] md:w-[50rem] p-3 text-center">
            <p className="font-bold text-2xl mt-5">{profile.username}</p>
            <p className="border-t-4 border-t-blue-500 mt-2"></p>
            <p className="font-semibold mt-12">
              <i className="fas fa-map-marked-alt text-sky-700 "></i> Alamat : { profile.alamatSekolah}
            </p>
            <p className="font-semibold mt-1">
              <i className="fas fa-envelope-open-text text-sky-700"></i> Email :
              {profile.email}
            </p>
            <p className="font-semibold mt-1">
              <i className="fas fa-envelope-open-text text-sky-700"></i> Nama Sekolah :
              {profile.namaSekolah}
            </p>
            <p className="font-semibold mt-1"> <i className="fas fa-phone-square-alt text-sky-700"></i> No Telephone : {profile.noTelepon}</p>

            <button className="bg-blue-400 text-white font-bold py-2 px-4 mt-4" type="button">
             <a href={"/editprofile/" + profile.id}><i className="fas fa-edit"></i> Edit</a>
            </button>
          </div>
          <i></i>
        </div>
        <div className="bg-blue-400 text-white md:w-[8rem] w-[5.3rem] md:p-2 p-1 text-center absolute md:ml-[13rem] ml-[8.1rem] -mt-[19.5rem] md:shadow shadow-2xl md:-mt-5 rounded-2xl hover:rounded-tl-none hover:rounded-br-none transition hover:duration-100">
          <button className="md:text-base text-sm" type="button" onClick={handleShow}>Ganti Foto</button>
        </div>
      </div>
      <Modal
              show={show}
              onHide={handleClose}
              id="authentication-modal"
              tabindex="-1"
              aria-hidden="true"
              className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
            >
              <div className="relative w-full h-full max-w-md md:h-auto">
                <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
                  <button
                    type="button"
                    className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                    data-modal-hide="authentication-modal"
                    onClick={handleClose}
                  >
                    <svg
                      aria-hidden="true"
                      className="w-5 h-5"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                        clip-rule="evenodd"
                      ></path>
                    </svg>
                    <span className="sr-only">Close modal</span>
                  </button>
                  <div className="px-6 py-6 lg:px-8">
                    <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-white">
                      Edit
                    </h3>
                    <form
                      onSubmit={put}
                      className="space-y-3"
                      // onSubmit={add}
                    >
                      <div>
                        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                          Foto Profile
                        </label>
                        <input
                          placeholder="Foto Profile"
                          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                          required
                          type="file"
                          onChange={(e) => setPhotoProfile(e.target.files[0])}
                        />
                      </div>
                      <button
                        type="submit"
                        className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                      >
                        Edit
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </Modal>

            {/* Modal Password */}
            <Modal
              show={show1}
              onHide={handleClose1}
              id="authentication-modal"
              tabindex="-1"
              aria-hidden="true"
              className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
            >
              <div className="relative w-full h-full max-w-md md:h-auto">
                <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
                  <button
                    type="button"
                    className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                    data-modal-hide="authentication-modal"
                    onClick={handleClose1}
                  >
                    <svg
                      aria-hidden="true"
                      className="w-5 h-5"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                        clip-rule="evenodd"
                      ></path>
                    </svg>
                    <span className="sr-only">Close modal</span>
                  </button>
                  <div className="px-6 py-6 lg:px-8">
                    <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-white">
                      Edit
                    </h3>
                    <form
                      onSubmit={put1}
                      className="space-y-3"
                      // onSubmit={add}
                    >
                      <div>
                        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                          Password
                        </label>
                        <input
                          placeholder="Password"
                          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
                          required
                          type="password"
                          onChange={(e) => setPassword(e.target.value)}
                        />
                      </div>
                      <button
                        onClick={handleClose}
                        type="submit"
                        className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                      >
                        Edit
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </Modal>
      </div></>
     ) : (
       <></>
     )}
    </div>
  );
}