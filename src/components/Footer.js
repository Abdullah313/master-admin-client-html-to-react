import React from "react";

const Footer = () => {
  return (
    <div>
      <footer className="main-footer">
        <strong>
          Copyright © 2022-2023 <a href="">Binus Programming</a>.
        </strong>
        All rights reserved.
        <div className="float-right d-none d-sm-inline-block">
          <b>Version</b> 1.1.0
        </div>
      </footer>
    </div>
  );
};

export default Footer;
