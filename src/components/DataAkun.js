import axios from "axios";
import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { useHistory } from "react-router";
import Swal from "sweetalert2";
import Navbar from "./Navbar";
import Sidebar1 from "./Sidebar1";

export default function () {
  const [show, setShow] = useState(false);

  const [nilai, setNilai] = useState([]);
  const [totalPages, setTotalPages] = useState([]);
  const [search, setSearch] = useState([]);

  const handleClose = () => setShow(false);
  const handleClose1 = () => setShow(false);
  const handleShow = () => setShow(true);

  const [namaSekolah, setNamaSekolah] = useState("");
  const [username, setUsername] = useState("");
  const [alamatSekolah, setAlamatSekolah] = useState("");
  const [noTelepon, setNoTelepon] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");

  const history = useHistory();

  const add = async (e) => {
    e.preventDefault();
    try {
      await axios
        .post("http://localhost:2026/sekolah/register", {
          namaSekolah: namaSekolah,
          username: username,
          alamatSekolah: alamatSekolah,
          noTelepon: noTelepon,
          email: email,
          password: password,
          role: role,
        })
        .then(() => {
          Swal.fire({
            icon: "success",
            title: "Berhasil Registrasi",
            showConfirmButton: false,
            timer: 1500,
          });
          setTimeout(() => {
            // history.push('/dataAkun')
            window.location.reload();
          }, 1500);
        });
    } catch (error) {
      alert("Terjadi Kesalahan " + error);
    }
  };

  const getAll = async (page = 0) => {
    await axios
      .get(`http://localhost:2026/sekolah?page=${page}&query=${search}`)
      .then((res) => {
        const pages = [];
        for (let index = 0; index < res.data.data.totalPages; index++) {
          pages.push(index);
        }
        setTotalPages(pages);
        setSearch(search);
        setNilai(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  return (
    <div>
      {localStorage.getItem("userId") !== null ? (
       <>
      {localStorage.getItem("role") === "SUPERADMIN" ? (
        <>
          <Navbar />
          <div className="flex gap-10 md:gap-0">
            <Sidebar1 />
            <div className="p-3 w-full overflow-y-auto h-[90vh] sidebar">
              <div>
                <h1 className="text-xl font-bold">Data Akun</h1>
              </div>
              <hr className="h-1 bg-slate-500 mt-2 rounded-t" />
              <div className="shadow md:mb-1">
                <div className="mt-2 p-3 md:ml-[90%]">
                  <button
                    data-modal-target="authentication-modal"
                    data-modal-toggle="authentication-modal"
                    type="submit"
                    className="bg-green-700 hover:bg-green-800 text-white md:h-9 h-6 w-20 md:w-24 rounded"
                    onClick={handleShow}
                  >
                    <i class="fas fa-plus"></i> Tambah
                  </button>
                </div>

                <div className=" p-2">
                  {/* search */}
                  <div className="pb-4 md:flex">
                    <div className="flex gap-2 ml-auto">
                      <button
                        class="block text-grey-darker text-base mt-1"
                        onClick={() => getAll()}
                      >
                        Search
                      </button>
                      <input
                        class="shadow-sm appearance-none border rounded w-[10rem] md:h-9 py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                        type="search"
                        aria-label="Search"
                        value={search}
                        onChange={(e) => setSearch(e.target.value)}
                      />
                    </div>
                  </div>

                  <table className="min-w-[20%] text-center border py-4">
                    <thead class="text-xs text-gray-700 bg-white dark:bg-white dark:text-gray-400">
                      <tr>
                        <th
                          scope="col"
                          className="md:text-sm text-xs font-medium text-gray-900 w-60 md:py-4 py-2"
                        >
                          No.
                        </th>
                        <th
                          scope="col"
                          className="md:text-sm text-xs font-medium text-gray-900 w-96 px-6 md:py-4 py-2"
                        >
                          Username
                        </th>
                        <th
                          scope="col"
                          className="md:text-sm text-xs font-medium text-gray-900 w-96 px-6 md:py-4 py-2"
                        >
                          Email
                        </th>
                        <th
                          scope="col"
                          className="md:text-sm text-xs font-medium text-gray-900 w-96 px-6 md:py-4 py-2"
                        >
                          Role
                        </th>
                        <th
                          scope="col"
                          className="md:text-sm text-xs font-medium text-gray-900 w-96 px-6 md:py-4 py-2"
                        >
                          Password
                        </th>
                      </tr>
                    </thead>
                    <tbody className="bg-gray-100">
                      {nilai.map((mapell, index) => (
                        <tr key={mapell.id} className="border-b">
                          <td className="px-3 md:py-4 py-2  whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                            {index + 1}
                          </td>
                          <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2  whitespace-nowrap">
                            {mapell.username}
                          </td>
                          <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2  whitespace-nowrap">
                            {mapell.email}
                          </td>
                          <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2  whitespace-nowrap">
                            {mapell.role}
                          </td>
                          <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2  whitespace-nowrap">
                            {mapell.password}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                  <div className="ml-auto">
                    <nav aria-label="Page navigation example ml-auto">
                      <ul className="md:h-8 h-5 mt-4 inline-flex items-center px-2 py-1 text-sm font-medium text-gray-700 hover:text-gray-500 bg-slate-100 hover:bg-slate-50 border-gray-500 border">
                        <li>
                          <a className="pointer-events-none relative block rounded bg-transparent py-1.5 px-3 text-sm text-gray-700 transition-all duration-300">
                            Previous
                          </a>
                        </li>
                        {totalPages.map((pages, index) => (
                          <li aria-current="page" key={index}>
                            <button
                              className="relative block rounded bg-primary-100 md:py-1.5 md:px-3 text-sm font-medium text-primary-700 transition-all duration-300"
                              onClick={() => getAll(pages)}
                            >
                              {pages + 1}
                            </button>
                          </li>
                        ))}
                        <li>
                          <a
                            className="relative block rounded bg-transparent py-1.5 px-3 text-sm text-gray-700 transition-all duration-300"
                            href="#!"
                          >
                            Next
                          </a>
                        </li>
                      </ul>
                    </nav>
                  </div>

                  {/* modal add */}
                  <Modal
                    show={show}
                    onHide={handleClose}
                    id="authentication-modal"
                    tabindex="-1"
                    aria-hidden="true"
                    className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
                  >
                    <div className="relative w-full h-full max-w-md md:h-auto">
                      <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
                        <button
                          type="button"
                          className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                          data-modal-hide="authentication-modal"
                          onClick={handleClose}
                        >
                          <svg
                            aria-hidden="true"
                            className="w-5 h-5"
                            fill="currentColor"
                            viewBox="0 0 20 20"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              fill-rule="evenodd"
                              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                              clip-rule="evenodd"
                            ></path>
                          </svg>
                          <span className="sr-only">Close modal</span>
                        </button>
                        <div className="px-6 py-6 lg:px-8">
                          <h3 className="mb-4 md:text-xl text-base font-medium text-black dark:text-black">
                            Tambah
                          </h3>
                          <form className="space-y-3" onSubmit={add}>
                            <div>
                              <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                                Nama Sekolah
                              </label>
                              <input
                                placeholder="Masukkan Nama Sekolah"
                                onChange={(e) => setNamaSekolah(e.target.value)}
                                value={namaSekolah}
                                className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                                required
                              />
                            </div>
                            <div>
                              <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                                Username
                              </label>
                              <input
                                placeholder="Masukkan Username"
                                onChange={(e) => setUsername(e.target.value)}
                                value={username}
                                className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                                required
                              />
                            </div>
                            <div>
                              <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                                Alamat Sekolah
                              </label>
                              <input
                                placeholder="Masukkan Alamat Sekolah"
                                onChange={(e) =>
                                  setAlamatSekolah(e.target.value)
                                }
                                value={alamatSekolah}
                                className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                                required
                              />
                            </div>
                            <div>
                              <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                                Nomor Telepon
                              </label>
                              <input
                                placeholder="0"
                                onChange={(e) => setNoTelepon(e.target.value)}
                                value={noTelepon}
                                className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                                required
                              />
                            </div>
                            <div>
                              <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                                Email
                              </label>
                              <input
                                placeholder="masukkan email"
                                onChange={(e) => setEmail(e.target.value)}
                                value={email}
                                className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                                required
                              />
                            </div>
                            <div>
                              <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                                Password
                              </label>
                              <input
                                placeholder="masukkan password"
                                onChange={(e) => setPassword(e.target.value)}
                                value={password}
                                className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                                required
                              />
                            </div>
                            <div className="my-4">
                              <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                                Role
                              </label>
                              <select
                                onChange={(e) => setRole(e.target.value)}
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                              >
                                <option selected>Masuk Sebagai</option>
                                <option value={"SUPERADMIN"}>
                                  Super Admin
                                </option>
                                <option value={"ADMIN"}>Admin</option>
                                <option value={"AKADEMIK"}>Akademik</option>
                                <option value={"PERPUSTAKAAN"}>
                                  Perpustakaan
                                </option>
                                <option value={"GURU"}>guru</option>
                                <option value={"TU"}>Tata Usaha</option>
                              </select>
                            </div>
                            <button
                              onClick={handleClose}
                              type="submit"
                              className="w-full text-black bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                            >
                              Tambah
                            </button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </Modal>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : (
        <></>
      )}
     </>
     ) : (
       <></>
     )}</div>
  );
}
