import "../App.css";
import React from "react";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

const SideNav = () => {
  const [open, setOpen] = useState(false);
  const history = useHistory();
  const logout = () => {
    Swal.fire({
      title: "Anda Yakin Ingin Logout",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          icon: "success",
          title: "Berhasil Logout",
          showConfirmButton: false,
          timer: 1500,
        });
        //Untuk munuju page selanjutnya
        localStorage.clear();
      }
      history.push("/");
    });
    // .then(() => {
    //   setTimeout(() => {
    //     window.location.reload();
    //     localStorage.clear();
    //   }, 1500);
    // });
  };
  return (
    <div>
      {/* Main Sidebar Container */}
      <aside className="main-sidebar sidebar-dark-primary elevation-4">
        {/* Brand Logo */}
        <a href="#" className="brand-link">
          <img
            src="dist/img/SIS.jpg"
            alt="Admin SIS Logo"
            className="brand-image img-circle elevation-3"
            style={{ opacity: ".8" }}
          />
          <span className="brand-text font-weight-light namaApp">
            Sistem Informasi Sekolah
          </span>
        </a>
        {/* Sidebar */}
        <div className="sidebar">
          {/* Sidebar user panel (optional) */}
          <div className="user-panel mt-3 pb-3 mb-3 d-flex">
            <div className="image">
              <img
                src="dist/img/logoSekolah.jpeg"
                className="img-circle elevation-2"
                alt="User Image"
                style={{ width: "40px", height: "40px" }}
              />
            </div>
            <div className="info">
              <a href="#" className="d-block">
                SMK INDONESIA
              </a>
            </div>
          </div>

          {/* Sidebar Menu */}
          <nav className="mt-2">
            <ul
              className="nav nav-pills nav-sidebar flex-column"
              data-widget="treeview"
              role="menu"
              data-accordion="false"
            >
              {/* Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library */}
              {/* SIDEBAR AKADEMIK */}
              {localStorage.getItem("role") === "ADMIN" ||
              localStorage.getItem("role") === "SUPERADMIN" ||
              localStorage.getItem("role") === "AKADEMIK" ? (
                <>
                  <li className="nav-item menu-open">
                    <a href="#" className="nav-link ">
                      <i className="nav-icon fas fa-solid fa-graduation-cap" />
                      <p>
                        AKADEMIK
                        <i className="right fas fa-angle-left" />
                      </p>
                    </a>
                    <ul className="nav nav-treeview">
                      <li className="nav-item">
                        <a href="/tahunajaran" className="nav-link ">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Tahun Ajaran</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/rombel" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Rombongan Belajar</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/mapel" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Mata Pelajaran</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/kelas" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Kelas</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/jenjang" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Jenjang</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/jenismapel" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>
                            Jenis Mata Pelajaran
                          </p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/guru" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Guru</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/pendaftaransiswa" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Pendaftaran Siswa</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/pembagiankelas" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Pembagian Kelas</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/siswa" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Siswa</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/mutasi" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Mutasi</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                </>
              ) : (
                <></>
              )}

              {/* SIDEBAR PERPUSTAKAAN */}
              {localStorage.getItem("role") === "ADMIN" ||
              localStorage.getItem("role") === "SUPERADMIN" ||
              localStorage.getItem("role") === "PERPUSTAKAAN" ? (
                <>
                  <li className="nav-item menu-open">
                    <a href="#" className="nav-link ">
                      <i className="nav-icon fas fa-book" />
                      <p>
                        PERPUSTAKAAN
                        <i className="right fas fa-angle-left" />
                      </p>
                    </a>
                    <ul className="nav nav-treeview">
                      <li className="nav-item">
                        <a href="/rakbuku" className="nav-link ">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Data Rak Buku</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/kategoribuku" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Data Kategory Buku</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/databuku" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Data Buku</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/dataanggota" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Data Anggota</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/peminjaman" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Peminjaman</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/pengembalian" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Pengembalian</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/laporan" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Laporan</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                </>
              ) : (
                <></>
              )}
              {/* SIDEBAR NILAI */}
              {localStorage.getItem("role") === "ADMIN" ||
              localStorage.getItem("role") === "SUPERADMIN" ||
              localStorage.getItem("role") === "GURU" ? (
                <>
                  <li className="nav-item menu-open">
                    <a href="#" className="nav-link ">
                      <i className="nav-icon fas fa-solid fa-marker" />
                      <p>
                        NILAI
                        <i className="right fas fa-angle-left" />
                      </p>
                    </a>
                    <ul className="nav nav-treeview">
                      <li className="nav-item">
                        <a href="/inputnilai" className="nav-link ">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Input Nilai</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/datanilai" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Data Nilai</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/cetakraport" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Cetak Raport</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                </>
              ) : (
                <></>
              )}
              {/* SIDEBAR KEUANGAN */}
              {localStorage.getItem("role") === "ADMIN" ||
              localStorage.getItem("role") === "SUPERADMIN" ||
              localStorage.getItem("role") === "TU" ? (
                <>
                  <li className="nav-item menu-open">
                    <a href="#" className="nav-link ">
                      <i className="nav-icon fas fa-solid fa-dollar-sign" />
                      <p>
                        KEUANGAN
                        <i className="right fas fa-angle-left" />
                      </p>
                    </a>
                    <ul className="nav nav-treeview">
                      <li className="nav-item">
                        <a href="/rencanaanggaran" className="nav-link ">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Rencana Anggaran</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/datamasukkeluar" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>
                            Input Dana Masuk/Keluar
                          </p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/jurnal" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Jurnal</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/laporanjurnal" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Laporan Jurnal</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/laporanbukubesar" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Laporan Buku Besar</p>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="/laporanneraca" className="nav-link">
                          <i className="far fa-circle nav-icon" />
                          <p style={{ fontSize: "15px" }}>Laporan Neraca</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                </>
              ) : (
                <></>
              )}
              <li className="nav-item" onClick={logout}>
                <a href="#" className="nav-link ">
                  <i className="nav-icon fas fas fa-sign-out-alt" />
                  <p>Logout</p>
                </a>
              </li>
            </ul>
          </nav>
          {/* /.sidebar-menu */}
        </div>
        {/* /.sidebar */}
      </aside>
      {/* Control Sidebar */}
      <aside className="control-sidebar control-sidebar-dark">
        {/* Control sidebar content goes here */}
      </aside>
      {/* /.control-sidebar */}
    </div>
  );
};

export default SideNav;
