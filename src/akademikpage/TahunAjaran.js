import { Modal } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import { useParams } from "react-router-dom";

function TahunAjaran() {
  const [show, setShow] = useState(false);
  const param = useParams();

  const handleClose = () => setShow(false);
  const handleClose1 = () => setShow(false);
  const handleShow = () => setShow(true);

  const [keterangan, setKeterangan] = useState("");
  const [akhirPriode, setAkhirPriode] = useState("");
  const [awalPriode, setAwalPriode] = useState("");
  const [kodeTahunAjaran, setKodeTahunAjaran] = useState("");
  const [nama, setNama] = useState("");
  const [status, setStatus] = useState("");

  const [tahun, setTahun] = useState([]);
  const [totalPages, setTotalPages] = useState([]);
  const [tahun_ajaran, setTahunAjaran] = useState("");

  const getAll = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2026/tahun_ajaran?page=${page}&search=${tahun_ajaran}`
      )
      .then((res) => {
        const pages = [];
        for (let index = 0; index < res.data.data.totalPages; index++) {
          pages.push(index);
        }
        setTotalPages(pages);
        setTahun(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll(0);
  }, []);

  const add = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.post(
        `http://localhost:2026/tahun_ajaran?akhirPriode=${akhirPriode}&awalPriode=${awalPriode}&keterangan=${keterangan}&kodeTahunAjaran=${kodeTahunAjaran}&nama=${nama}&status=${status}`
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses menambahkan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const deleteUser = async (id) => {
    Swal.fire({
      title: "Menghapus?",
      text: "Anda mengklik tombol!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#5F8D4E",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Hapus ini!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`http://localhost:2026/tahun_ajaran/` + id);
        window.location.reload();
      }
    });
  };
  return (
    <div>
      {localStorage.getItem("role") === "ADMIN" ||
      localStorage.getItem("role") === "SUPERADMIN" ||
      localStorage.getItem("role") === "AKADEMIK" ? (
        <>
          {/* Content Wrapper. Contains page content */}
          <div className="content-wrapper">
            {/* Content Header (Page header) */}
            <section className="content-header">
              <div className="container-fluid">
                <div className="row mb-2">
                  <div className="col-sm-6">
                    <h1>Tahun Ajaran</h1>
                  </div>
                  <div className="col-sm-6">
                    <ol className="breadcrumb float-sm-right">
                      <li className="breadcrumb-item">
                        <a href="/home">Home</a>
                      </li>
                      <li className="breadcrumb-item active">Tahun Ajaran</li>
                    </ol>
                  </div>
                </div>
              </div>
              {/* /.container-fluid */}
            </section>
            {/* Main content */}
            <section className="content">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-12">
                    <div className="card">
                      <div className=" d-flex align-items-center justify-content-between border-bottom py-1 px-4 ">
                        <h3 className="card-title">Data Tahun Ajaran</h3>
                        {/* <button
                          className="btn btn-success"
                          type="submit"
                          onClick={handleShow}
                        >
                          Tambah
                        </button> */}
                        <button
                          type="button"
                          class="btn btn-success"
                          data-toggle="modal"
                          data-target="#exampleModalCenter"
                        >
                          Tambah
                        </button>
                      </div>
                      {/* /.card-header */}
                      <div className="card-body">
                        <table
                          id="example1"
                          className="table table-bordered table-striped"
                        >
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Kode Tahun Ajaran</th>
                              <th>Nama</th>
                              <th>Awal Periode</th>
                              <th>Akhir periode</th>
                              <th>Keterangan</th>
                              <th>Status</th>
                              <th>Aksi</th>
                            </tr>
                          </thead>
                          <tbody>
                            {tahun.map((tahun_ajaran, index) => {
                              return (
                                <tr key={index} className="border-b">
                                  <td className="px-6 py-2 whitespace-nowrap md:text-base text-xs font-medium text-gray-900 ">
                                    {index + 1}
                                  </td>
                                  <td className="md:text-base text-xs text-gray-900 font-light px-6 py-2 whitespace-nowrap ">
                                    {tahun_ajaran.kodeTahunAjaran}
                                  </td>
                                  <td className="md:text-base text-xs text-gray-900 font-light px-6 py-2 whitespace-nowrap ">
                                    {tahun_ajaran.nama}
                                  </td>
                                  <td className="md:text-base text-xs text-gray-900 font-light px-6 py-2 whitespace-nowrap ">
                                    {tahun_ajaran.awalPriode}
                                  </td>
                                  <td className="md:text-base text-xs text-gray-900 font-light px-6 py-2 whitespace-nowrap ">
                                    {tahun_ajaran.akhirPriode}
                                  </td>
                                  <td className="md:text-base text-xs text-gray-900 font-light px-6 py-2 whitespace-nowrap ">
                                    {tahun_ajaran.keterangan}
                                  </td>
                                  <td className="md:text-base text-xs text-gray-900 font-light px-6 py-2 whitespace-nowrap ">
                                    {tahun_ajaran.status}
                                  </td>
                                  <td className="md:text-base text-xs md:ml-[9%] text-gray-900 font-light px-6 py-2 whitespace-nowrap flex gap-3">
                                    <button className="bg-blue-500 hover:bg-blue-700 md:text-sm text-xs text-white font-bold py-1 px-2 rounded">
                                      <a
                                        href={
                                          "/editTahunAjaran/" + tahun_ajaran.id
                                        }
                                      >
                                        <i class="fas fa-edit"></i> Edit
                                      </a>
                                    </button>
                                    <div className="border w-16">
                                      <button
                                        onClick={() =>
                                          deleteUser(tahun_ajaran.id)
                                        }
                                        className="bg-red-500 hover:bg-red-700 text-white font-bold md:text-sm text-xs py-1 px-2 rounded"
                                      >
                                        <i class="fas fa-trash-alt"></i> Hapus
                                      </button>
                                    </div>
                                  </td>
                                </tr>
                              );
                            })}
                          </tbody>
                        </table>
                      </div>

                      {/* /.card-body */}

                      {/* Modal-Start */}
                      <div
                        class="modal fade"
                        id="exampleModalCenter"
                        tabindex="-1"
                        role="dialog"
                        aria-labelledby="exampleModalCenterTitle"
                        aria-hidden="true"
                      >
                        <div
                          class="modal-dialog modal-dialog-centered"
                          role="document"
                        >
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5
                                class="modal-title"
                                id="exampleModalLongTitle"
                              >
                                Tambah Data
                              </h5>
                              <button
                                type="button"
                                class="close"
                                data-dismiss="modal"
                                aria-label="Close"
                              >
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              {/* Form */}
                              <form className="space-y-3" onSubmit={add}>
                                <div className="form-group-sm">
                                  <label htmlFor="kode" className="text-sm">
                                    Kode Tahun Ajaran
                                  </label>
                                  <input
                                    type="text"
                                    className="form-control text-sm"
                                    id="kode"
                                    placeholder="Kode Tahun Ajaran"
                                    onChange={(e) =>
                                      setKodeTahunAjaran(e.target.value)
                                    }
                                    value={kodeTahunAjaran}
                                    required
                                  />
                                </div>
                                <div className="form-group-sm">
                                  <label htmlFor="kode" className="text-sm">
                                    Nama
                                  </label>
                                  <input
                                    type="text"
                                    className="form-control text-sm"
                                    id="nama"
                                    placeholder="Nama"
                                    onChange={(e) => setNama(e.target.value)}
                                    value={nama}
                                    required
                                  />
                                </div>
                                <div className="form-group-sm">
                                  <label htmlFor="kode" className="text-sm">
                                    Awal Periode
                                  </label>
                                  <input
                                    type="text"
                                    className="form-control text-sm"
                                    id="awalperiode"
                                    placeholder="Awal Periode"
                                    onChange={(e) =>
                                      setAwalPriode(e.target.value)
                                    }
                                    value={awalPriode}
                                    required
                                  />
                                </div>
                                <div className="form-group-sm">
                                  <label htmlFor="kode" className="text-sm">
                                    Akhir periode
                                  </label>
                                  <input
                                    type="text"
                                    className="form-control text-sm"
                                    id="akhirperiode"
                                    placeholder="Akhir periode"
                                    onChange={(e) =>
                                      setAkhirPriode(e.target.value)
                                    }
                                    value={akhirPriode}
                                    required
                                  />
                                </div>
                                <div className="form-group-sm">
                                  <label htmlFor="kode" className="text-sm">
                                    Keterangan
                                  </label>
                                  <input
                                    type="text"
                                    className="form-control text-sm"
                                    id="keterangan"
                                    placeholder="Keterangan"
                                    onChange={(e) =>
                                      setKeterangan(e.target.value)
                                    }
                                    value={keterangan}
                                    required
                                  />
                                </div>
                                <div className="form-group-sm">
                                  <label htmlFor="kode" className="text-sm">
                                    Status
                                  </label>
                                  <input
                                    type="text"
                                    className="form-control text-sm"
                                    id="keterangan"
                                    placeholder="Status"
                                    onChange={(e) => setStatus(e.target.value)}
                                    value={status}
                                    required
                                  />
                                </div>
                                <div class="d-flex justify-content-end  pt-2">
                                  <button
                                    type="button"
                                    class="btn btn-secondary m-1"
                                    data-dismiss="modal"
                                  >
                                    Batal
                                  </button>
                                  <button
                                    type="submit"
                                    class="btn btn-primary m-1"
                                  >
                                    Tambahkan
                                  </button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                      {/* modal add */}
                      {/* <Modal
                        show={show}
                        onHide={handleClose}
                        id="authentication-modal"
                        tabindex="-1"
                        aria-hidden="true"
                        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
                      >
                        <div className="relative w-full h-full max-w-md md:h-auto">
                          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
                            <button
                              type="button"
                              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                              data-modal-hide="authentication-modal"
                              onClick={handleClose}
                            >
                              <svg
                                aria-hidden="true"
                                className="w-5 h-5"
                                fill="currentColor"
                                viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  fill-rule="evenodd"
                                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                  clip-rule="evenodd"
                                ></path>
                              </svg>
                              <span className="sr-only">Close modal</span>
                            </button>
                            <div className="px-6 py-6 lg:px-8">
                              <h3 className="mb-4 md:text-xl text-base font-medium text-black dark:text-black">
                                Tambahkan
                              </h3>
                              <form className="space-y-3" onSubmit={add}>
                                <div>
                                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                                    Kode Tahun Ajaran
                                  </label>
                                  <input
                                    placeholder="Kode Tahun Ajaran"
                                    onChange={(e) =>
                                      setKodeTahunAjaran(e.target.value)
                                    }
                                    value={kodeTahunAjaran}
                                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                                    required
                                  />
                                </div>
                                <div>
                                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                                    Nama
                                  </label>
                                  <input
                                    placeholder="Nama"
                                    onChange={(e) => setNama(e.target.value)}
                                    value={nama}
                                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                                    required
                                  />
                                </div>
                                <div>
                                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                                    Awal Periode
                                  </label>
                                  <input
                                    placeholder="Awal Periode"
                                    onChange={(e) =>
                                      setAwalPriode(e.target.value)
                                    }
                                    value={awalPriode}
                                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                                    required
                                  />
                                </div>
                                <div>
                                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                                    Akhir periode
                                  </label>
                                  <input
                                    placeholder="Akhir periode"
                                    onChange={(e) =>
                                      setAkhirPriode(e.target.value)
                                    }
                                    value={akhirPriode}
                                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                                    required
                                  />
                                </div>
                                <div>
                                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                                    Keterangan
                                  </label>
                                  <input
                                    placeholder="Keterangan"
                                    onChange={(e) =>
                                      setKeterangan(e.target.value)
                                    }
                                    value={keterangan}
                                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                                    required
                                  />
                                </div>
                                <div>
                                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                                    Status
                                  </label>
                                  <input
                                    placeholder="Status"
                                    onChange={(e) => setStatus(e.target.value)}
                                    value={status}
                                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                                    required
                                  />
                                </div>
                                <button
                                  onClick={handleClose}
                                  type="submit"
                                  className="w-full text-black bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                >
                                  Tambah
                                </button>
                              </form>
                            </div>
                          </div>
                        </div>
                      </Modal> */}
                      {/* Modal-End */}
                    </div>
                    {/* /.card */}
                  </div>
                  {/* /.col */}
                </div>
                {/* /.row */}
              </div>
              {/* /.container-fluid */}
            </section>
            {/* /.content */}
          </div>
          {/* /.content-wrapper */}
        </>
      ) : (
        <></>
      )}
    </div>
  );
}

export default TahunAjaran;
