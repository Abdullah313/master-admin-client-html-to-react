import React from "react";

function PembagianKelas() {
  return (
    <div>
      {/* Content Wrapper. Contains page content */}
      <div className="content-wrapper">
        {/* Content Header (Page header) */}
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>Seleksi dan Pembagian Kelas</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <a href="/home">Home</a>
                  </li>
                  <li className="breadcrumb-item active">Seleksi</li>
                  <li className="breadcrumb-item active">Pembagian Kelas</li>
                </ol>
              </div>
            </div>
          </div>
          {/* /.container-fluid */}
        </section>
        {/* Main content */}
        <section className="content">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-header">
                    <h3 className="card-title">Data Pembagian Kelas</h3>
                  </div>
                  {/* /.card-header */}
                  <div className="card-body">
                    <table
                      id="example1"
                      className="table table-bordered table-striped"
                    >
                      <thead>
                        <tr>
                          <th>
                            <div class="custom-control custom-checkbox">
                              <input
                                class="form-check-input"
                                type="checkbox"
                                id="checkboxNoLabel"
                              />
                            </div>
                          </th>
                          <th>No Reg</th>
                          <th>Tahun Ajaran</th>
                          <th>Jenjang</th>
                          <th>Nama</th>
                          <th>Jekel</th>
                          <th>Tempat Lahir</th>
                          <th>Tanggal Lahir</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                  {/* /.card-body */}
                </div>
                {/* /.card */}
              </div>
              {/* /.col */}
            </div>
            {/* /.row */}
          </div>
          {/* /.container-fluid */}
        </section>
        {/* /.content */}
      </div>
      {/* /.content-wrapper */}
    </div>
  );
}

export default PembagianKelas;
