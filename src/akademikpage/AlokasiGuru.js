import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import Navbar from "../component/Navbar";
import Sidebar from "../component/Sidebar";
import Sidebar1 from "../component/Sidebar1";

export default function AlokasiGuru() {
  const [tombol, setTombol] = useState();
  const [tombol1, setTombol1] = useState();

  const param = useParams();
  const history = useHistory();
    const [list, setList] = useState([]);
    const [totalPages, setTotalPages] = useState([]);
    const [alokasi_guru, setAlokasiGuru] = useState("");
    const [namaPelajaran, setNamaPelajaran] = useState("");
    const [guru, setGuru] = useState([]);
    const [search, setSearch] = useState("");

  const getAll = async (page = 0) => {
  
    await axios
      .get(
        `http://localhost:2026/mata_pelajaran/all?page=${page}&search=${search}`
      )
      .then((res) => {
        const pages = [];
        for (let index = 0; index < res.data.data.totalPages; index++) {
          pages.push(index);
        }
        setTotalPages(pages);
        setList(
          res.data.data.content
        );
        // console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };



  
  const Guru = async () => {
    await axios
      .get("http://localhost:2026/guru/" + param.id)
      .then((res) => {
        setGuru(res.data.data);
    

        // sessionStorage.setItem("Mapel", param.id);
      })
      .catch((err) => {
        // console.log(err);
      });
    // window.location.reload();
  };

  const [list1, setList1] = useState([]);
  const [pages1, setPages1] = useState(0);
  const getAll1 = async (idx) => {
    await axios
      .get(
        `http://localhost:2026/alokasi_guru/all?guru=${param.id}&page=${idx}&search=${search}`
      )
      .then((res) => {
        setPages1(res.data.data.totalPages);
        setList1(
          res.data.data.content.map((item) => ({
            ...item,
            qty: 1,
          }))
        );
        // console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const add = (alokasi_guru) => {
    axios.post(
      `http://localhost:2026/alokasi_guru/post?guru=${param.id}&mataPelajaran=${tombol}`,
      {
        // usersId: localStorage.getItem("userId"),
      }
    );

    Swal.fire("Berhasil!", "Di masukan Ke keranjang", "success");
  };

  const hapus = async (alokasi_guru) => {
    Swal.fire({
      title: "Menghapus?",
      text: "Anda mengklik tombol!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#5F8D4E",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Hapus ini!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:2026/alokasi_guru/"+ tombol1);
      }
      window.location.reload();
    });
  };
  const keluar = () => {
    // sessionStorage.clear();
    // localStorage.clear();
    history.push("/dasbord");
    window.location.reload();
  };

 
  const handleChange1 = (Baru) => {
    if (Baru != "benar") {
      if (tombol1 != true) {
        console.log(Baru)
        setTombol1(Baru);
      }
    }
  };
  useEffect(() => {
    getAll(0);
    getAll1(0);
   Guru();

  }, []);

  const handleChange = (Baru) => {
    if (Baru != "benar") {
        // console.log(Baru)
        setTombol(Baru);
      
    }
  };


  console.log(tombol1);
  return (
    
    <div>
       {localStorage.getItem("userId") !== null ? (
       <>
        {localStorage.getItem("role") ==="ADMIN"|| localStorage.getItem("role") ===
      "SUPERADMIN" ||localStorage.getItem("role") ==="AKADEMI" ? (
      <>
      <Navbar/>
      <div className="flex  gap-x-4">
        <div>
          <Sidebar1/>
          </div>
        <div class="mx-auto">
          <div className="flex justify-between">
            <h2 class="text-2xl font-bold leading-tight mt-5">Alokasi Guru</h2>
            <div className="mt-4 text-gray-500">
            <a
              className="md:text-sm text-xs ml-auto text-blue-600"
              href="/dashboard"
            >
              Dashboard
              <span className="text-black">
                <button 
                // onClick={keluar}
                >
                  <a href="/mataPelajaran"> / Mapel / Alokasi Mapel</a>
                </button>
              </span>
            </a>
            </div>
          </div>
          <div class="py-3 border border-t-4 border-t-gray-500 p-6 mt-7 shadow-lg flex justify-between gap-x-[10rem]">
            <div className="">
              <div className="flex gap-x-[47rem]">
                <h2 class="text-base text-gray-500 leading-tight font-medium">
                  Alokasi Mata Pelajaran
                  <i class="fa-sharp fa-solid fa-arrow-right"></i> Guru
                </h2>
              </div>
              <div className="flex justify-around">
                <p className="text-lg mt-4 border-b-2 w-[19rem] text-gray-800">
                  Data Mata Pelajaran
                </p>
                <hr className="h-10" />
                <button    onClick={() => add(alokasi_guru)} class="rounded-lg px-4 py-2 ml-[5.8rem] mt-4  bg-green-700 text-green-100 hover:bg-green-800 duration-300">
                  Simpan
                </button>
              </div>
              <div>
                <div class="mb-4 ml-[16.6rem] flex mt-5 gap-x-3">
                  <label
                    class="block text-grey-darker text-sm font-bold mt-3"
                   htmlFor="search"
                  >
                    Search
                  </label>
                  <input
                    class="shadow appearance-none border rounded w-[10rem] py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                    id="username"
                    type="text"
                  />
                </div>
              </div>
              <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4   overflow-x-auto w-fit">
                <div class="inline-block  shadow-md overflow-hidden w-[30rem] -mt-3">
                  <table class="leading-normal border w-[30rem]">
                    <thead className="text-center ">
                      <tr>
                        <th
                          scope="col"
                          className="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider"
                        >
                          <div class="custom-control custom-checkbox">
                            <input
                              type="checkbox"
                              class="custom-control-input"
                              id="customCheck2"
                            />
                          </div>
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                          Nama Mapel
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                          Keterangan
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    {list.map((mapel) => {
                        return (
                      <tr>
                        <td
                          scope="col"
                          className="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm"
                        >
                          <div class="custom-control custom-checkbox">
                            <input
                              type="checkbox"
                              class="custom-control-input"
                              id="customCheck2"
                              onChange={() => handleChange(mapel.id)}
                            />
                          </div>
                        </td>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">
                            {mapel.namaPelajaran}
                          </p>
                        </td>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">{mapel.keterangan}</p>
                        </td>
                      </tr>
                      )})}
                    </tbody>
                  </table>
                </div>
              </div>

              <div class="bg-white md:float-right flex items-center flex-wrap">
                <nav aria-label="Page navigation">
                  <ul class="inline-flex">
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 rounded-l-lg focus:shadow-outline hover:bg-green-100">
                        Prev
                      </button>
                    </li>
                    <li className="flex">
                    
                    {totalPages.map((pages, index) => {
                            return (
                              <li aria-current="page" key={index}>
                                <button
                                  className="relative block border-blue-700 border bg-primary-100 md:py-2.5 md:px-3 text-sm font-medium text-primary-700 transition-all duration-300"
                                  onClick={() => getAll(pages)}
                                >
                                  {pages + 1}
                                </button>
                              </li>
                            )
                          })}
                      </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-blue-600 rounded-r-lg focus:shadow-outline hover:bg-green-100">
                        Next
                      </button>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>

            <div className=" w-fit mt-5">
              <p className="text-right font-bold">{guru.nama}</p>
              <div className="flex w-fit justify-between gap-x-[3.2rem]">
           
                <p className="text-lg mt-4 border-b-2 w-[19rem] text-gray-800">
                  Data Alokasi Ke Guru
                </p>
                <hr className="h-10" />
                <button onClick={() => hapus(alokasi_guru)} class="rounded-lg px-4 py-2 mt-4  bg-red-700 text-green-100 hover:bg-green-800 duration-300">
                  Hapus
                </button>
              </div>
              <div class="mb-4 ml-[16.6rem] flex mt-5 gap-x-3 w-fit">
                <label
                  class="block text-grey-darker text-sm font-bold mt-3"
                  htmlFor="search"
                >
                  Search
                </label>
                <input
                  class="shadow appearance-none border rounded w-[10rem] py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                  id="username"
                  type="text"
                />
              </div>
              <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 w-fit overflow-x-auto">
                <div class="inline-block  shadow-md overflow-hidden w-fit -mt-3">
                  <table class="leading-normal w-fit">
                    <thead className="text-center ">
                      <tr>
                        <th
                          scope="col"
                          className="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider"
                        >
                          <div class="custom-control custom-checkbox">
                            <input
                              type="checkbox"
                              class="custom-control-input"
                              id="customCheck2"
                            />
                          </div>
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                          Nama Mapel
                        </th>
                        <th class="px-12 font-bold py-3 border-b-2 border-gray-200  text-left text-xs  text-gray-700 uppercase tracking-wider">
                          Keterangan
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    {list1.map((guru, index) => {
                        return (
                      <tr>
                        <td
                          scope="col"
                          className="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm"
                        >
                          <div class="custom-control custom-checkbox">
                            <input
                              type="checkbox"
                              class="custom-control-input"
                              id="customCheck2"
                              value={tombol}
                                    onChange={() => handleChange1(guru.id)}
                            />
                          </div>
                        </td>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">
                            {guru.mataPelajaran.namaPelajaran}
                          </p>
                        </td>
                        <td class="px-12 py-5 border-b border-gray-200 bg-gray-200 text-sm">
                          <p class="text-gray-900 whitespace-no-wrap">{guru.mataPelajaran.keterangan}</p>
                        </td>
                      </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
              </div>

              <div class=" p-4 flex items-center md:float-right ml-[12.5rem] -mt-3  w-fit">
                <nav aria-label="Page navigation">
                  <ul class="inline-flex">
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-r-0 border-blue-600 rounded-l-lg focus:shadow-outline hover:bg-green-100">
                        Prev
                      </button>
                    </li>
                    <li className="flex">
                    
                    {totalPages.map((pages, index) => {
                            return (
                              <li aria-current="page" key={index}>
                                <button
                                  className="relative block border-blue-700 border bg-primary-100 md:py-2.5 md:px-3 text-sm font-medium text-primary-700 transition-all duration-300"
                                  onClick={() => getAll(pages)}
                                >
                                  {pages + 1}
                                </button>
                              </li>
                            );
                          })}
                      </li>
                    <li>
                      <button class="px-4 py-2 text-black transition-colors duration-150 bg-white border border-blue-600 rounded-r-lg focus:shadow-outline hover:bg-green-100">
                        Next
                      </button>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
      </>):(<></>
      )}
      </>
      ) : (
        <></>
      )} </div>
    
  );
}
     