import React, { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import Sidebar from "../component/Sidebar";
import Navbar from "../component/Navbar";
import Sidebar1 from "../component/Sidebar1";
import axios from "axios";
import Footer from "../component/Footer";

export default function () {
  const [nilai, setNilai] = useState([]);
  const [search, setSearch] = useState([]);
  const [totalPages, setTotalPages] = useState([]);

  const getAll = async (page = 0) => {
    await axios
      .get(
        `http://localhost:2026/input-nilai/all?page=${page}&search=${search}`
      )
      .then((res) => {
        const pages = [];
        for (let index = 0; index < res.data.data.totalPages; index++) {
          pages.push(index);
        }
        setTotalPages(pages);
        setSearch(search);
        setNilai(
          res.data.data.content.map((data) => ({
            ...data,
          }))
        );
      })
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  return (
    <div>
      {localStorage.getItem("role") === "ADMIN" ||
      localStorage.getItem("role") === "SUPERADMIN" ||
      localStorage.getItem("role") === "GURU" ? (
        <>
          <Navbar />
          <div className="flex gap-10 md:gap-0">
            <Sidebar1 />
            <div className="p-3 w-full overflow-y-auto h-[90vh] sidebar">
              <div className="md:flex px-2 mt-1">
                <p className="md:w-80 md:text-2xl text-sm">
                  <b>Data Nilai</b>
                </p>
                <a className="md:text-sm ml-auto text-xs text-blue-600" href="">
                  Dashboard
                  <span className="text-black "> / Data Nilai</span>
                </a>
              </div>
              <hr className="h-1 bg-slate-500 rounded-t" />
              <div className="shadow md:mb-1">
                <div className="p-3 font-light md:text-lg">
                  <b>Pilih Mapel & Rombel</b>
                </div>
                <div className="md:flex md:gap-10 p-4">
                  {/* content 1 */}
                  <div>
                    <div className="flex md:gap-20 gap-20 pb-4">
                      <p className=" font-bold text-gray-500 md:text-lg text-sm">
                        Mata Pelajaran yang diampu
                        <hr className="bg-black md:w-72 w-32" />
                      </p>
                    </div>
                    <div className="pb-4 md:flex">
                      <div className="flex gap-1">
                        <h1 className="mt-1">Show</h1>
                        <Form.Select className="w-16 h-8">
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </Form.Select>
                        <h1 className="mt-1">Entries</h1>
                      </div>

                      <div className="flex gap-2 ml-auto">
                        <button
                          className="block text-grey-darker text-base mt-1"
                          onClick={() => getAll()}
                        >
                          Search
                        </button>
                        <input
                          className="shadow-sm appearance-none border rounded w-[10rem] md:h-9 py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                          type="search"
                          aria-label="Search"
                          value={search}
                          onChange={(e) => setSearch(e.target.value)}
                        />
                      </div>
                    </div>
                    <div className="">
                      <table className="min-w-[20%] text-center mt-2 border py-4">
                        <thead className="text-xs text-gray-700 bg-white dark:bg-white  dark:text-gray-400">
                          <tr>
                            <th
                              scope="col"
                              className="md:text-sm text-xs font-medium text-gray-900 w-60 md:py-4 py-2"
                            >
                              No.
                            </th>
                            <th
                              scope="col"
                              className="md:text-sm text-xs font-medium text-gray-900 w-96 px-6 md:py-4 py-2"
                            >
                              Mapel
                            </th>
                            <th
                              scope="col"
                              className="md:text-sm text-xs font-medium text-gray-900 w-96 px-6 md:py-4 py-2"
                            >
                              Aksi
                            </th>
                          </tr>
                        </thead>
                        <tbody className="bg-gray-100">
                          {nilai.map((mapell, index) => (
                            <tr key={mapell.id} className="border-b">
                              <td className="px-3 md:py-4 py-2  whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                                {index + 1}
                              </td>
                              <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2  whitespace-nowrap">
                                {mapell.idMapel.namaPelajaran}
                              </td>
                              <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2 whitespace-nowrap">
                                <div>
                                  <a href="/dataN">
                                    <button className="mx-1 bg-gray-200 w-9 h-7 rounded border">
                                      <i className="fas fa-greater-than text-xs"></i>{" "}
                                    </button>
                                  </a>
                                </div>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                      <div className="flex md:gap-40 gap-[20%]">
                        <p className="md:text-sm text-xs w-64 text-gray-600 py-3">
                          Showing 1 to 1 of 1 entries
                        </p>

                        <nav aria-label="Page navigation example ml-auto">
                          <ul className="md:h-8 h-5 mt-4 inline-flex items-center px-2 py-1 text-sm font-medium text-gray-700 hover:text-gray-500 bg-slate-100 hover:bg-slate-50 border-gray-500 border">
                            <li>
                              <a className="pointer-events-none relative block rounded bg-transparent py-1.5 px-3 text-sm text-gray-700 transition-all duration-300">
                                Previous
                              </a>
                            </li>
                            {totalPages.map((pages, index) => (
                              <li aria-current="page" key={index}>
                                <button
                                  className="relative block rounded bg-primary-100 md:py-1.5 md:px-3 text-sm font-medium text-primary-700 transition-all duration-300"
                                  onClick={() => getAll(pages)}
                                >
                                  {pages + 1}
                                </button>
                              </li>
                            ))}
                            <li>
                              <a
                                className="relative block rounded bg-transparent py-1.5 px-3 text-sm text-gray-700 transition-all duration-300"
                                href="#!"
                              >
                                Next
                              </a>
                            </li>
                          </ul>
                        </nav>
                      </div>
                    </div>
                  </div>

                  {/* content 2 */}
                  <div className="mt-9 md:mt-0">
                    <div className="flex pb-4 md:gap-0 gap-12">
                      <p className="md:w-52 font-bold text-gray-500 md:text-lg text-sm">
                        Alokasi Rombel
                        <hr className="bg-black md:w-80 w-40" />
                      </p>
                    </div>
                    <div className="pb-4 md:flex">
                      <div className="flex gap-1">
                        <h1 className="mt-1">Show</h1>
                        <Form.Select className="w-16 h-8">
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </Form.Select>
                        <h1 className="mt-1">Entries</h1>
                      </div>

                      <div className="flex gap-2 ml-auto">
                        <button
                          className="block text-grey-darker text-base mt-1"
                          onClick={() => getAll()}
                        >
                          Search
                        </button>
                        <input
                          className="shadow-sm appearance-none border rounded w-[10rem] md:h-9 py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                          type="search"
                          aria-label="Search"
                          value={search}
                          onChange={(e) => setSearch(e.target.value)}
                        />
                      </div>
                    </div>

                    <div className="">
                      <table className="min-w-[20%] text-center mt-2 border py-4">
                        <thead className="text-xs text-gray-700 bg-white dark:bg-white  dark:text-gray-400">
                          <tr>
                            <th
                              scope="col"
                              className="md:text-sm text-xs font-medium text-gray-900 w-60 md:py-4 py-2"
                            >
                              No.
                            </th>
                            <th
                              scope="col"
                              className="md:text-sm text-xs font-medium text-gray-900 w-96 px-6 md:py-4 py-2"
                            >
                              Rombel
                            </th>
                            <th
                              scope="col"
                              className="md:text-sm text-xs font-medium text-gray-900 w-96 px-6 md:py-4 py-2"
                            >
                              Aksi
                            </th>
                          </tr>
                        </thead>
                        <tbody className="bg-gray-100">
                          {nilai.map((rombell, index) => (
                            <tr key={rombell.id} className="border-b">
                              <td className="px-3 md:py-4 py-2  whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                                {index + 1}
                              </td>
                              <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2  whitespace-nowrap">
                                {rombell.idRombel.namaRombonganBelajar}
                              </td>
                              <td className="md:text-sm text-xs text-gray-900 font-light px-3 md:py-4 py-2 whitespace-nowrap">
                                <div>
                                  <button
                                    className="mx-1 bg-green-700 w-14 h-7 rounded text-white"
                                    // onClick={Ganjil}
                                  >
                                    Ganjil
                                  </button>
                                  <button
                                    className="mx-1 bg-green-700 w-14 h-7 rounded text-white"
                                    // onClick={Genap}
                                  >
                                    Genap
                                  </button>
                                </div>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>

                      <div className="flex md:gap-[22%] gap-[20%]">
                        <p className="md:text-sm text-xs w-64 text-gray-600 py-3">
                          Showing 1 to 1 of 1 entries
                        </p>

                        <nav aria-label="Page navigation example ml-auto">
                          <ul className="md:h-8 h-5 mt-4 inline-flex items-center px-2 py-1 text-sm font-medium text-gray-700 hover:text-gray-500 bg-slate-100 hover:bg-slate-50 border-gray-500 border">
                            <li>
                              <a className="pointer-events-none relative block rounded bg-transparent py-1.5 px-3 text-sm text-gray-700 transition-all duration-300">
                                Previous
                              </a>
                            </li>
                            {totalPages.map((pages, index) => (
                              <li aria-current="page" key={index}>
                                <button
                                  className="relative block rounded bg-primary-100 md:py-1.5 md:px-3 text-sm font-medium text-primary-700 transition-all duration-300"
                                  onClick={() => getAll(pages)}
                                >
                                  {pages + 1}
                                </button>
                              </li>
                            ))}
                            <li>
                              <a
                                className="relative block rounded bg-transparent py-1.5 px-3 text-sm text-gray-700 transition-all duration-300"
                                href="#!"
                              >
                                Next
                              </a>
                            </li>
                          </ul>
                        </nav>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="mt-[8rem]">
                <Footer />
              </div>{" "}
            </div>
          </div>
        </>
      ) : (
        <>
          <center>
            <div className="border shadow p-5">
              <h1>401 Not Found</h1>
              <h1>Halaman Tidak Tersedia</h1>
            </div>
          </center>
        </>
      )}
    </div>
  );
}
