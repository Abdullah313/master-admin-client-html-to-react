import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import Navbar from "../component/Navbar";
import Sidebar from "../component/Sidebar";
import Sidebar1 from "../component/Sidebar1";

export default function () {
  const [nilai, setNilai] = useState([]);
  const [mbuhh, setMbuhh] = useState([]);
  const [mbuh, setMbuh] = useState([]);
  const [mbuhhh, setMbuhhh] = useState([]);
  const [namaSis, setNamaSis] = useState([]);
  const [niss, setNiss] = useState([]);
  const [nuh1, setNuh1] = useState("");
  const [nuh2, setNuh2] = useState("");
  const [nuh3, setNuh3] = useState("");
  const [nt1, setNt1] = useState("");
  const [nt2, setNt2] = useState("");
  const [nt3, setNt3] = useState("");
  const [mid, setMid] = useState("");
  const [rnuh, setRnuh] = useState("");
  const [nh, setNh] = useState("");
  const [smt, setSmt] = useState("");
  const [rnt, setRnt] = useState("");
  const [nar, setNar] = useState("");
  const param = useParams();

  const entry = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:2026/nilai", {
        nuh1: nuh1,
        nuh2: nuh2,
        nuh3: nuh3,
        nt1: nt1,
        nt2: nt2,
        nt3: nt3,
        mid: mid,
        rnuh: rnuh,
        nh: nh,
        smt: smt,
        rnt: rnt,
        nar: nar,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  const history = useHistory();

  const getAll = async () => {
    await axios
      .get("http://localhost:2026/nilai/all")
      .then((res) => {
        setNilai(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
  };
  useEffect(() => {
    getAll();
  }, []);

  useEffect(() => {
    axios
      .get("http://localhost:2026/nilai/" + param.id)
      .then((response) => {
        const entri = response.data.data;
        setNamaSis(entri.siswa);
        setNiss(entri.siswa);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);

  useEffect(() => {
    axios
      .get("http://localhost:2026/input-nilai/" + param.id)
      .then((response) => {
        const entri = response.data.data;
        setMbuh(entri.idRombel);
        setMbuhh(entri.idMapel);
        setMbuhhh(entri);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);

  // const getid = async (id) => {
  //   await axios
  //     .get("http://localhost:2026/input-nilai/" + id)
  //     .then((res) => {
  //       setMbuhh(res.data.data);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  //     getid(0);
  // };

  const update = async (e) => {
    try {
      await axios.put(
        `http://localhost:2026/nilai/${localStorage.getItem("userId")}`,
        {
          nuh1: nuh1,
          nuh2: nuh2,
          nuh3: nuh3,
          nt1: nt1,
          nt2: nt2,
          nt3: nt3,
          mid: mid,
          rnuh: rnuh,
          nh: nh,
          smt: smt,
          rnt: rnt,
          nar: nar,
        }
      );
      Swal.fire({
        icon: "success",
        title: "berhasil mengedit profile",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {}, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div>
      {localStorage.getItem("role") === "ADMIN" ||
      localStorage.getItem("role") === "SUPERADMIN" ||
      localStorage.getItem("role") === "GURU" ? (
        <>
          <Navbar />
          <div className="flex  md:gap-x-0 gap-x-11">
            <Sidebar1 />
            <div className="p-3 w-full overflow-y-auto h-[90vh] sidebar">
              <div className="md:flex p-2">
                <p className="md:w-80 md:text-2xl text-sm">
                  <b>Entry</b>
                </p>
                <a className="md:text-sm ml-auto text-xs text-blue-600" href="">
                  Dashboard
                  <span className="text-black "> / Input Nilai / Entry</span>
                </a>
              </div>
              <hr className="h-1 bg-slate-500 mt-2 rounded-t" />
              <div className="shadow md:mb-1">
                <div className=" gap-5 p-5">
                  {/* content 1 */}
                  <div>
                    <div className="flex md:gap-20 gap-20">
                      <div>
                        <h1 className="md:text-lg">{mbuhh.namaPelajaran}</h1>
                        <p className=" font-bold text-gray-500 md:text-lg text-sm">
                          {mbuh.namaRombonganBelajar} ({mbuhhh.smt})
                          <hr className="bg-black md:w-72 w-52" />
                        </p>
                      </div>
                    </div>
                    <div className="">
                      <table className="w-full text-center mt-3 py-4">
                        <thead className="text-xs md:text-sm h-12 uppercase text-gray-700 bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                          <tr className="">
                            <th scope="col" className="md:px-6 px-4 md:py-3 py-2">
                              No.
                            </th>
                            <th scope="col" className="md:px-6 px-4 md:py-3 py-2">
                              NIS
                            </th>
                            <th scope="col" className="md:px-6 px-4 md:py-3 py-2">
                              Nama Siswa
                            </th>
                            <th
                              scope="col"
                              className="md:px-6 px-4 md:py-3 py-2 md:w-20"
                            ></th>
                          </tr>
                        </thead>
                        <tbody>
                          {nilai.map((mapell, index) => (
                            <tr
                              key={mapell.id}
                              className="bg-white border-b dark:bg-gray-900 dark:border-gray-700"
                            >
                              <td className="md:px-6 md:py-4">{index + 1}</td>
                              <td className="md:px-6 md:py-4">
                                {mapell.siswa.nisn}
                              </td>
                              <td className="md:px-6 md:py-4">
                                {mapell.siswa.nama}
                              </td>
                              <td className="md:px-6 md:py-4">
                                <div className="md:flex md:gap-2 md:p-1">
                                  <div>
                                    <button
                                      className="mx-1 bg-red-600 hover:bg-red-700 w-9 h-7 rounded text-white"
                                      type="submit"
                                      onClick={() => update(mapell)}
                                    >
                                      <i className="fas fa-greater-than text-xs"></i>{" "}
                                    </button>
                                  </div>
                                </div>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </div>

                  <form onSubmit={update}>
                    {/* content 2 */}
                    <div className="md:p-5 mt-5 ">
                      <div className="flex gap-5">
                        <div className="">
                          <div className="flex gap-5 ">
                            <div className="font-bold text-right">
                              <h1>Nama</h1>
                              <h1 className="mt-3">NIS</h1>
                            </div>
                            <div className="">
                              <h1>{namaSis.nama}</h1>
                              <h1 className="mt-3">{niss.nisn}</h1>
                            </div>
                          </div>
                          <div className="md:flex md:gap-20 md:w-1/2 mt-3">
                            <div className="flex gap-5 md:mt-2">
                              <div className="text-right font-bold">
                                <h1 className="mt-2">NUH1</h1>
                                <h1 className="mt-8">NUH2</h1>
                                <h1 className="mt-10">NUH3</h1>
                                <h1 className="mt-8">MID</h1>
                                <h1 className="mt-8">RNUH</h1>
                                <h1 className="mt-8">NH</h1>
                              </div>
                              <div className="">
                                <input
                                  className="h-9 border"
                                  type="text"
                                  value={nuh1}
                                  onChange={(e) => setNuh1(e.target.value)}
                                />
                                <input
                                  className="h-9 mt-6 border"
                                  type="text"
                                  value={nuh2}
                                  onChange={(e) => setNuh2(e.target.value)}
                                />
                                <input
                                  className="h-9 mt-6 border"
                                  type="text"
                                  value={nuh3}
                                  onChange={(e) => setNuh3(e.target.value)}
                                />
                                <input
                                  className="h-9 mt-6 border"
                                  type="text"
                                  value={mid}
                                  onChange={(e) => setMid(e.target.value)}
                                />
                                <input
                                  className="h-9 mt-6 border"
                                  type="text"
                                  value={rnuh}
                                  onChange={(e) => setRnuh(e.target.value)}
                                />
                                <input
                                  className="h-9 mt-6 border"
                                  type="text"
                                  value={nh}
                                  onChange={(e) => setNh(e.target.value)}
                                />
                              </div>
                            </div>

                            <div className="flex gap-5 mt-2 md:mt-2">
                              <div className="text-right ml-3 font-bold">
                                <h1 className="mt-2">NT1</h1>
                                <h1 className=" mt-8">NT2</h1>
                                <h1 className="mt-8">NT3</h1>
                                <h1 className="mt-8">SMT</h1>
                                <h1 className="mt-8">RNT</h1>
                                <h1 className="mt-8">NAR</h1>
                              </div>
                              <div className="">
                                <input
                                  className="h-9 border"
                                  type="text"
                                  value={nt1}
                                  onChange={(e) => setNt1(e.target.value)}
                                />
                                <input
                                  className="h-9 mt-6 border"
                                  type="text"
                                  value={nt2}
                                  onChange={(e) => setNt2(e.target.value)}
                                />
                                <input
                                  className="h-9 mt-6 border"
                                  type="text"
                                  value={nt3}
                                  onChange={(e) => setNt3(e.target.value)}
                                />
                                <input
                                  className="h-9 mt-6 border"
                                  type="text"
                                  value={smt}
                                  onChange={(e) => setSmt(e.target.value)}
                                />
                                <input
                                  className="h-9 mt-6 border"
                                  type="text"
                                  value={rnt}
                                  onChange={(e) => setRnt(e.target.value)}
                                />
                                <input
                                  className="h-9 mt-6 border"
                                  type="text"
                                  value={nar}
                                  onChange={(e) => setNar(e.target.value)}
                                />
                                <button
                                  className="flex gap-2 mt-3 bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                  type="submit"
                                >
                                  <i className="fas fa-cloud-download-alt"> </i>
                                  Simpan
                                </button>
                              </div>
                            </div>
                          </div>
                          <div className="flex items-center justify-between mt-3 ml-16"></div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : (
        <>
          {" "}
          <center>
            <div className="border shadow p-5">
              <h1>401 Not Found</h1>
              <h1>Halaman Tidak Tersedia</h1>
            </div>
          </center>
        </>
      )}
    </div>
  );
}
