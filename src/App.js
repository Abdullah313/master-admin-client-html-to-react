import Header from "./components/Header";
import Footer from "./components/Footer";
import SideNav from "./components/SideNav";
import Dashboard from "./page/Home";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import TahunAjaran from "./akademikpage/TahunAjaran";
import RombonganBelajar from "./akademikpage/RombonganBelajar";
import MataPelajaran from "./akademikpage/MataPelajaran";
import Kelas from "./akademikpage/Kelas";
import Jenjang from "./akademikpage/Jenjang";
import JenisMataPelajaran from "./akademikpage/JenisMataPelajaran";
import Guru from "./akademikpage/Guru";
import PendaftaranSiswa from "./akademikpage/PendaftaranSiswa";
import PembagianKelas from "./akademikpage/PembagianKelas";
import Siswa from "./akademikpage/Siswa";
import Mutasi from "./akademikpage/Mutasi";
// perpustakaan import file
import DataRakBuku from "./perpustakaanpage/DataRakBuku";
import DataKategoriBuku from "./perpustakaanpage/DataKategoriBuku";
import DataBuku from "./perpustakaanpage/DataBuku";
import DataAnggota from "./perpustakaanpage/DataAnggota";
import Peminjaman from "./perpustakaanpage/Peminjaman";
import Pengembalian from "./perpustakaanpage/Pengembalian";
import Laporan from "./perpustakaanpage/Laporan";
// nilai import file
import InputNilai from "./nilaipage/InputNilai";
import DataNilai from "./nilaipage/DataNilai";
import CetakRaport from "./nilaipage/CetakRaport";
// login
import Login from "./page/Login";
import Register from "./page/Register";
// keuangan import file
import RencanaAnggaran from "./keuanganpage/RencanaAnggaran";
import Akun from "./keuanganpage/Akun";
import InputDataMasukKeluar from "./keuanganpage/InputDataMasukKeluar";
import Jurnal from "./keuanganpage/Jurnal";
import LaporanJurnal from "./keuanganpage/LaporanJurnal";
import LaporanBukuBesar from "./keuanganpage/LaporanBukuBesar";
import LaporanNeraca from "./keuanganpage/LaporanNeraca";
import Home from "./page/Dashboard";
function App() {
  const isAuthenticated = true; // variabel ini harus diisi dengan kondisi apakah pengguna telah login atau belum

  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/" component={Login} exact />
          <Route path="/registerAkun" component={Register} exact />
          {localStorage.getItem("role") !== null ? (
            <>
              <div>
                <Header />
                <div>
                  <Route path="/home" component={Home} />
                  <Route path="/dashboard" component={Dashboard} />
                  {/* akademik path */}
                  <Route path="/tahunajaran" component={TahunAjaran} />
                  <Route path="/rombel" component={RombonganBelajar} />
                  <Route path="/mapel" component={MataPelajaran} />
                  <Route path="/kelas" component={Kelas} />
                  <Route path="/jenjang" component={Jenjang} />
                  <Route path="/jenismapel" component={JenisMataPelajaran} />
                  <Route path="/guru" component={Guru} />
                  <Route
                    path="/pendaftaransiswa"
                    component={PendaftaranSiswa}
                  />
                  <Route path="/pembagiankelas" component={PembagianKelas} />
                  <Route path="/siswa" component={Siswa} />
                  <Route path="/mutasi" component={Mutasi} />
                  {/* perpustakaan path */}
                  <Route path="/rakbuku" component={DataRakBuku} />
                  <Route path="/kategoribuku" component={DataKategoriBuku} />
                  <Route path="/databuku" component={DataBuku} />
                  <Route path="/dataanggota" component={DataAnggota} />
                  <Route path="/peminjaman" component={Peminjaman} />
                  <Route path="/pengembalian" component={Pengembalian} />
                  <Route path="/laporan" component={Laporan} />
                  {/* nilai path */}
                  <Route path="/inputnilai" component={InputNilai} />
                  <Route path="/datanilai" component={DataNilai} />
                  <Route path="/cetakraport" component={CetakRaport} />
                  {/* keuangan path */}
                  <Route path="/rencanaanggaran" component={RencanaAnggaran} />
                  <Route path="/akun" component={Akun} />
                  <Route
                    path="/datamasukkeluar"
                    component={InputDataMasukKeluar}
                  />
                  <Route path="/jurnal" component={Jurnal} />
                  <Route path="/laporanjurnal" component={LaporanJurnal} />
                  <Route
                    path="/laporanbukubesar"
                    component={LaporanBukuBesar}
                  />
                  <Route path="/laporanneraca" component={LaporanNeraca} />
                </div>
                <SideNav />
                <Footer />
              </div>
            </>
          ) : (
            <>
              <Redirect to="/" />
            </>
          )}
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
