import React, { useState } from "react";
import Navbar from "../component/Navbar";
import Sidebar1 from "../component/Sidebar1";

export default function () {
  const [nilai, setNilai] = useState([]);
  return (
    <div>
      {localStorage.getItem("userId") !== null ? (
       <>
      <Navbar />
    <div className="flex md:gap-0 gap-3">
      <div>
        <Sidebar1 />
      </div>
    <div className="md:px-10 w-full p-12">
      <div className="text-2xl font-semibold">Rekap Nilai Raport</div>
      <hr className="mt-2" />
      <div className="flex mt-3 text-xl">
        <div className="flex gap-5">
          <div className="md:text-lg text-base">
            <h1>Nama</h1>
            <h1>Kelas / Rombel</h1>
            <h1>Semester</h1>
          </div>
          <div  className="md:text-lg text-base">
            <h1>:</h1>
            <h1>:</h1>
            <h1>:</h1>
          </div>
        </div>
        <div>
          <h1></h1>
          <h1></h1>
          <h1></h1>
        </div>
      </div>
      <hr className="h-1 bg-black mt-3" />
      <table className="border-collapse mt-5 border-3 border-slate-400 md:w-full w-full">
        <thead>
          <tr>
            <th class="border border-slate-300 ...">No.</th>
            <th class="border border-slate-300 ...">Mata Pelajaran</th>
            <th class="border border-slate-300 ...">Nilai</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Matematika</td>
            <td>100</td>
          </tr>
        </tbody>
        <tbody>
          {nilai.map((mapel, index) => (
            <tr key={mapel.id} style={{ color: "black" }}>
              <td class="border border-slate-300 ...">{index + 1}</td>
              <td class="border border-slate-300 ..."></td>
              <td class="border border-slate-300 ..."></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
    </div>
    </>
     ) : (
       <></>
     )}
      </div>
  );
}
