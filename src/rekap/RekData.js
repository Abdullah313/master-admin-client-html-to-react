import React, { useState } from "react";
import Navbar from "../component/Navbar";
import Sidebar from "../component/Sidebar";
import Sidebar1 from "../component/Sidebar1";

export default function () {
  const [nilai, setNilai] = useState([]);
  return (
    <div>
       {localStorage.getItem("userId") !== null ? (
       <>
      <Navbar/>
    <div className="flex md:gap-x-0 gap-x-11">
      <div>
        <Sidebar1/>
      </div>
      <div className="p-3">
        <h1 className="md:text-center text-xl ">
          <b> Rekap Data Nilai</b>
        </h1>

        <h1 className="text-lg">Matematika</h1>
        <h1 className="mt-5">X TKJ 1 (Semester 1)</h1>
        <hr className="h-1 bg-black mt-5" />
        <table className="border-collapse border-3 border-slate-400 text-center py-2 px-4 w-full mt-2">
          <thead>
            <tr>
              <th class="md:w-10 border border-black ...">No.</th>
              <th class="md:w-20 border border-black ...">NIS</th>
              <th class="md:w-28 border border-black ...">Nama Siswa</th>
              <th class="md:w-20 border border-black ...">NUH1</th>
              <th class="md:w-20 border border-black ...">NUH2</th>
              <th class="md:w-20 border border-black ...">NUH3</th>
              <th class="md:w-20 border border-black ...">NT1</th>
              <th class="md:w-20 border border-black ...">NT2</th>
              <th class="md:w-20 border border-black ...">NT3</th>
              <th class="md:w-20 border border-black ...">MID</th>
              <th class="md:w-20 border border-black ...">SMT</th>
              <th class="md:w-20 border border-black ...">RNUH</th>
              <th class="md:w-20 border border-black ...">RNT</th>
              <th class="md:w-20 border border-black ...">NH</th>
              <th class="md:w-20 border border-black ...">NAR</th>
            </tr>
          </thead>
          <tbody>
            {nilai.map((mapel, index) => (
              <tr key={mapel.id} style={{ color: "black" }}>
                <td class="border border-black ...">{index + 1}</td>
                <td class="border border-black ..."></td>
                <td class="border border-black ..."></td>
                <td class="border border-black ..."></td>
                <td class="border border-black ..."></td>
                <td class="border border-black ..."></td>
                <td class="border border-black ..."></td>
                <td class="border border-black ..."></td>
                <td class="border border-black ..."></td>
                <td class="border border-black ..."></td>
                <td class="border border-black ..."></td>
                <td class="border border-black ..."></td>
                <td class="border border-black ..."></td>
                <td class="border border-black ..."></td>
                <td class="border border-black ..."></td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
    </>
     ) : (
       <></>
     )}
    </div>
  );
}
