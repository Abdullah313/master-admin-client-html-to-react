import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import Swal from "sweetalert2";

export default function EditKategori() {
  const [show, setShow] = useState(false);
  const [nama, setNama] = useState("");
  const [keterangan, setKeterangan] = useState("");
  const param = useParams();
  const history = useHistory();

  const editRak = async (e) => {
    e.preventDefault();

    try {
      await axios.put("http://localhost:2026/kategory/" + param.id, {
        nama: nama,
        keterangan: keterangan,
      });
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "berhasil mengedit",
        showConfirmButton: false,
      });
      setTimeout(() => {
        history.push("/kategori");
        window.location.reload();
      }, 1500);
    } catch (error) {
      alert("terjadi Kesalahan " + error);
    }
  };
  useEffect(() => {
    axios
      .get("http://localhost:2026/kategory/" + param.id)
      .then((response) => {
        const rak = response.data.data;
        setNama(rak.nama);
        setKeterangan(rak.keterangan);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);
  return (
    <div>
      {localStorage.getItem("role") === "ADMIN" ||
      localStorage.getItem("role") === "SUPERADMIN" ||
      localStorage.getItem("role") === "PERPUSTAKAAN" ? (
        <>
          <div className="shadow-2xl rounded-xl md:w-[35%] md:ml-[30%] mt-10 px-6 py-6 lg:px-8 bg-blue-500/10">
            <h3 className="mb-4 text-xl font-medium text-black dark:text-black">
              Edit
            </h3>
            <form className="space-y-3" onSubmit={editRak}>
              <div>
                <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                  Nama Kategori
                </label>
                <input
                  placeholder="Nama Kategori"
                  onChange={(e) => setNama(e.target.value)}
                  value={nama}
                  className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                  Keterangan
                </label>
                <input
                  placeholder=" Keterangan"
                  onChange={(e) => setKeterangan(e.target.value)}
                  value={keterangan}
                  className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <button
                type="submit"
                className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                Simpan
              </button>
            </form>
          </div>
        </>
      ) : (
        <></>
      )}
    </div>
  );
}
