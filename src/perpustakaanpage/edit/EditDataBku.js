import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function EditDataBuku() {
  const [rakBuku, setRakBuku] = useState([]);
  const [kategoriBuku, setKategoriBuku] = useState([]);

  const [nama, setNama] = useState("");
  const [kategory, setKategory] = useState("");
  const [rak, setRak] = useState("");
  const [pengarang, setPengarang] = useState("");
  const [penerbit, setPenerbit] = useState("");
  const [tahun, setTahun] = useState("");
  const [jumlah, setJumlah] = useState("");

  const [show, setShow] = useState(false);
  const history = useHistory();
  const param = useParams();

  const allRak = async (e) => {
    await axios
      .get(`http://localhost:2026/rak-buku/all`)
      .then((res) => {
        setRakBuku(res.data.data);
      })
      .catch((err) => {
        alert("Error" + err);
      });
  };

  const allKategori = async (e) => {
    await axios
      .get(`http://localhost:2026/kategory/all`)
      .then((res) => {
        setKategoriBuku(res.data.data);
      })
      .catch((err) => {
        alert("Error" + err);
      });
  };

  // METHOD PUT
  const putBuku = async (e) => {
    e.preventDefault();

    try {
      await axios.put("http://localhost:2026/data-buku/" + param.id, {
        nama: nama,
        kategory: kategory,
        rak: rak,
        pengarang: pengarang,
        penerbit: penerbit,
        tahun: tahun,
        jumlah: jumlah,
      });
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "berhasil mengedit",
        showConfirmButton: false,
      });
      setTimeout(() => {
        history.push("/buku");
        window.location.reload();
      }, 1500);
    } catch (error) {
      alert("terjadi Kesalahan " + error);
    }
  };

  useEffect(() => {
    const pp = axios
      .get("http://localhost:2026/data-buku/" + param.id)
      .then((res) => {
        const buku = res.data.data;
        setNama(buku.nama);
        setKategory(buku.kategory.nama);
        setRak(buku.rak.nama);
        setPengarang(buku.pengarang);
        setPenerbit(buku.penerbit);
        setTahun(buku.tahun);
        setJumlah(buku.jumlah);
      })
      .catch((err) => {
        alert("Error" + err);
      });
    allKategori();
    allRak();
  }, []);

  return (
    <div>
      {localStorage.getItem("role") === "ADMIN" ||
      localStorage.getItem("role") === "SUPERADMIN" ||
      localStorage.getItem("role") === "PERPUSTAKAAN" ? (
        <>
          {" "}
          <div className="shadow-2xl rounded-xl md:w-[35%] md:ml-[30%] mt-10 px-6 py-6 lg:px-8 bg-blue-500/10">
            <h3 className="mb-10 text-xl font-medium text-black dark:text-black">
              FORM EDIT BUKU
            </h3>
            <form className="space-y-5" onSubmit={putBuku}>
              <div className="flex justify-between items-center">
                <label className="block text-sm font-medium text-black dark:text-black w-48">
                  Judul
                </label>
                <input
                  placeholder="Judul"
                  className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  value={nama}
                  onChange={(e) => setNama(e.target.value)}
                  required
                />
              </div>
              <div className="flex justify-between items-center">
                <label className="block text-sm font-medium text-black dark:text-black w-48">
                  Kategori
                </label>
                <select
                  className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-black block w-fullp-1 dark:bg-white dark:border-gray-500 w-full p-1.5"
                  onChange={(e) => setKategory(e.target.value)}
                >
                  <option selected value={kategory}>{kategory}</option>
                  {kategoriBuku.map((data) => (
                    <option value={data.kategoriBuku} className="text-gray-400">
                      {data.nama}
                    </option>
                  ))}
                </select>
              </div>
              <div className="flex justify-between items-center">
                <label className="block text-sm font-medium text-black dark:text-black w-48">
                  Rak Buku
                </label>
                <select className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-black block w-fullp-1 dark:bg-white dark:border-gray-500 w-full p-1.5"
                onChange={(e) => setRak(e.target.value)}
                >
                  <option selected value={rak}>{rak}</option>
                  {rakBuku.map((data) => (
                    <option value={data.rakBuku} className="text-gray-400">
                      {data.nama}
                    </option>
                  ))}
                </select>
              </div>
              <div className="flex justify-between items-center">
                <label className="block text-sm font-medium text-black dark:text-black w-48">
                  Pengarang
                </label>
                <input
                  placeholder="Pengarang"
                  className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                  value={pengarang}
                  onChange={(e) => setPengarang(e.target.value)}
                />
              </div>
              <div className="flex justify-between items-center">
                <label className="block text-sm font-medium text-black dark:text-black w-48">
                  Penerbit
                </label>
                <input
                  placeholder="Penerbit"
                  className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                  value={penerbit}
                  onChange={(e) => setPenerbit(e.target.value)}
                />
              </div>
              <div className="flex justify-between items-center">
                <label className="block text-sm font-medium text-black dark:text-black w-48">
                  Tahun Terbit
                </label>
                <input
                  placeholder="Tahun terbit"
                  className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                  value={tahun}
                  onChange={(e) => setTahun(e.target.value)}
                />
              </div>
              <div className="flex justify-between items-center">
                <label className="block text-sm font-medium text-black dark:text-black w-48">
                  Jumlah Buku
                </label>
                <input
                  placeholder="Jumlah buku"
                  className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                  value={jumlah}
                  onChange={(e) => setJumlah(e.target.value)}
                />
              </div>

              <button
                type="submit"
                className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                Simpan
              </button>
            </form>
          </div>
        </>
      ) : (
        <></>
      )}
    </div>
  );
}
