import axios from "axios";
import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";

function RakPerpustakaan() {
  const [rakBuku, setRakBuku] = useState([]);
  const [search, setSearch] = useState("");
  const [totalPages, setTotalPages] = useState([]);
  const [nama, setNama] = useState("");
  const [keterangan, setKeterangan] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const getAllRak = async (page = 0) => {
    await axios
      .get(`http://localhost:2026/rak-buku?page=${page}&query=${search}`)
      .then((res) => {
        const pages = [];
        for (let index = 0; index < res.data.data.totalPages; index++) {
          pages.push(index);
        }
        setTotalPages(pages);
        setRakBuku(res.data.data.content);
        setSearch(search);
      })
      .catch((err) => {
        alert("Terjadi Kesalahan Sir " + err);
      });
  };

  useEffect(() => {
    getAllRak(0);
  }, []);
  return (
    <div>
      <h1>PAGE PENCARIAN BUKU</h1>
    </div>
  );
}

export default RakPerpustakaan;
